use cpu::CpuState;
use dsp::Dsp;
use util::*;
use dsp::DSPREG;
use bus::Addressable;

use std::fs::File;
use std::io::prelude::*;


pub struct LoadedFile {
    bin_data: [u8; 0x10200]
}

impl<'a> LoadedFile {

    pub fn from_disk(filename: String) -> LoadedFile {
        let mut f = File::open(filename).expect("File not found");
        let mut buf = [0u8; 0x10200];
        f.read_exact(&mut buf).unwrap();
        LoadedFile { bin_data: buf }
    }

    fn byte_at(&self, addr: usize) -> u8 {
        self.bin_data[addr]
    }

    fn word_at(&self, addr: usize) -> u16 {
        u16::hi_lo(self.bin_data[addr + 1], self.bin_data[addr])
    }

    fn dsp_register(&self, n: u8) -> u8 {
        if n > 0x7f { panic!("Nonexistent DSP register requested: {:02x}", n); }
        self.bin_data[0x10100 + (n as usize)]
    }

    pub fn cpu_state(&self) -> CpuState {
        CpuState {
            pc:  self.word_at(0x0025),
            a:   self.byte_at(0x0027),
            x:   self.byte_at(0x0028),
            y:   self.byte_at(0x0029),
            psw: self.byte_at(0x002a),
            sp:  self.byte_at(0x002b),
            busy_cycles: 0,
            halted: false
        }
    }

    pub fn set_dsp_state(&self, dsp: &mut Dsp) {
        for i in 0..128 {
            dsp.register_select = i;
            dsp.write_register(self.dsp_register(i));
        }
        // Rewrite Key On again, to be sure any voices that have been triggered on
        // have the latest values!! Failing to do this can cause the first note to be corrupted.
        dsp.register_select = DSPREG::KON;
        dsp.write_register(self.dsp_register(DSPREG::KON));
    }

    fn ram_at(&self, addr: u16) -> u8 {
        self.bin_data[0x100 + addr as usize]
    }

    pub fn ram(&self) -> [u8; 0x10000] {
        let mut ram = [0u8; 0x10000];
        for i in 0..0x10000 {
            ram[i] = self.ram_at(i as u16);
        }
        ram
    }

    pub fn reinit_system_registers(&self, bus: &Addressable) {
        // Rewrite the register values to ensure they are picked up...
        bus.write(0x00f0, self.ram_at(0x00f0));
        bus.write(0x00f1, self.ram_at(0x00f1));
        bus.write(0x00f2, self.ram_at(0x00f2));
        bus.write(0x00f3, self.ram_at(0x00f3));
        bus.write(0x00f4, self.ram_at(0x00f4));
        bus.write(0x00f5, self.ram_at(0x00f5));
        bus.write(0x00f6, self.ram_at(0x00f6));
        bus.write(0x00f7, self.ram_at(0x00f7));
        bus.write(0x00f8, self.ram_at(0x00f8));
        bus.write(0x00f9, self.ram_at(0x00f9));
        bus.write(0x00fa, self.ram_at(0x00fa));
        bus.write(0x00fb, self.ram_at(0x00fb));
        bus.write(0x00fc, self.ram_at(0x00fc));
        bus.write(0x00fd, self.ram_at(0x00fd));
        bus.write(0x00fe, self.ram_at(0x00fe));
        bus.write(0x00ff, self.ram_at(0x00ff));
    }

    pub fn game(&self) -> String {
        (self.bin_data[0x4e..0x6e].to_vec()).iter().take_while(|chr| **chr != 0).map(|chr: &u8| *chr as char).collect()
    }

    pub fn song_title(&self) -> String {
        (self.bin_data[0x2e..0x4e].to_vec()).iter().take_while(|chr| **chr != 0).map(|chr: &u8| *chr as char).collect()
    }

    pub fn composer(&self) -> String {
        // Depending on file format chosen (there are two possible) the composer will be either at b0-d0 or b1-d1
        // Unscientific method: if b0 contains a-z or A-Z use b0-d0 else b1-d1!
        let mut data = self.bin_data[0xb0..0xd1].to_vec();
        if (data[0] >= b'a' && data[0] <= b'z') || (data[0] >= b'A' && data[0] <= b'Z') {
            data = data[0..32].to_vec() // don't include the final byte [32] in the slice
        } else {
            data = data.drop(1); // don't include the initial byte in the slice
        }
        data.iter().take_while(|chr| **chr != 0).map(|chr: &u8| *chr as char).collect()
    }
}