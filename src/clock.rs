pub trait Clockable {
    fn tick(&mut self);
}
