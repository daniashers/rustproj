use util::*;
use bus::RAM;

pub struct Echo {
    pub buffer_start: u16,

    pub sample_capacity: u16,
    pub current_sample_offset: u16,

    pub filter_coefficients: [i8; 8]
}

impl Echo {
    pub fn new() -> Echo {
        Echo {
            buffer_start: 0,
            sample_capacity: 1,
            current_sample_offset: 0,
            filter_coefficients: [0i8; 8]
        }
    }

    #[inline]
    fn current_byte_offset(&self) -> u16 {
        self.current_sample_offset * 4 // each stereo sample is 4 bytes
    }

    pub fn byte_capacity(&self) -> u16 {
        self.sample_capacity * 4 // each stereo sample is 4 bytes
    }

    pub fn buffer_end(&self) -> u16 {
        self.buffer_start.wrapping_add(self.byte_capacity())
    }

    pub fn current_absolute_pointer(&self) -> u16 {
        self.buffer_start.wrapping_add(self.current_byte_offset())
    }

    #[inline]
    fn get_sample_at(&mut self, ram: &mut RAM, sample_position: u16) -> StereoSample {
        let pointer = self.buffer_start.wrapping_add(sample_position * 4);
        let l_lo = ram[(pointer + 0) as usize];
        let l_hi = ram[(pointer + 1) as usize];
        let r_lo = ram[(pointer + 2) as usize];
        let r_hi = ram[(pointer + 3) as usize];
        let l = u16::hi_lo(l_hi, l_lo) as i16;
        let r = u16::hi_lo(r_hi, r_lo) as i16;
        (l, r)
    }

    fn get_delayed_sample(&mut self, ram: &mut RAM, delay: u8) -> StereoSample {
        // delay = number of samples in the past.
        let mut sample_position: i32 = self.current_sample_offset as i32 - delay as i32;
        // bring back the position into the 'normal' range (i.e. allow for wrapping)
        if self.sample_capacity == 0 { sample_position = 0 } // sample capacity = 0 doesn't really exist, there is always at least 1 sample
        else {
            while sample_position < 0 { sample_position += self.sample_capacity as i32 }
        }
        self.get_sample_at(ram, sample_position as u16)
    }

    pub fn get_filtered_sample(&mut self, ram: &mut RAM) -> StereoSample {
        let s_minus_0 = self.get_delayed_sample(ram, 0).adjust_volume(self.filter_coefficients[0], self.filter_coefficients[0]);
        let s_minus_1 = self.get_delayed_sample(ram, 1).adjust_volume(self.filter_coefficients[1], self.filter_coefficients[1]);
        let s_minus_2 = self.get_delayed_sample(ram, 2).adjust_volume(self.filter_coefficients[2], self.filter_coefficients[2]);
        let s_minus_3 = self.get_delayed_sample(ram, 3).adjust_volume(self.filter_coefficients[3], self.filter_coefficients[3]);
        let s_minus_4 = self.get_delayed_sample(ram, 4).adjust_volume(self.filter_coefficients[4], self.filter_coefficients[4]);
        let s_minus_5 = self.get_delayed_sample(ram, 5).adjust_volume(self.filter_coefficients[5], self.filter_coefficients[5]);
        let s_minus_6 = self.get_delayed_sample(ram, 6).adjust_volume(self.filter_coefficients[6], self.filter_coefficients[6]);
        let s_minus_7 = self.get_delayed_sample(ram, 7).adjust_volume(self.filter_coefficients[7], self.filter_coefficients[7]);

        (0i16, 0i16)
            .saturating_add(s_minus_0)
            .saturating_add(s_minus_1)
            .saturating_add(s_minus_2)
            .saturating_add(s_minus_3)
            .saturating_add(s_minus_4)
            .saturating_add(s_minus_5)
            .saturating_add(s_minus_6)
            .saturating_add(s_minus_7)
    }

    pub fn put_sample(&mut self, ram: &mut RAM, sample: StereoSample) {
        let l_hi = (sample.0 as u16).hi();
        let l_lo = (sample.0 as u16).lo();
        let r_hi = (sample.1 as u16).hi();
        let r_lo = (sample.1 as u16).lo();
        ram[self.buffer_start.wrapping_add(self.current_byte_offset() + 0) as usize] = l_lo;
        ram[self.buffer_start.wrapping_add(self.current_byte_offset() + 1) as usize] = l_hi;
        ram[self.buffer_start.wrapping_add(self.current_byte_offset() + 2) as usize] = r_lo;
        ram[self.buffer_start.wrapping_add(self.current_byte_offset() + 3) as usize] = r_hi;
        self.current_sample_offset += 1;
        if self.current_sample_offset >= self.sample_capacity {
            self.current_sample_offset = 0;
        }
    }
}