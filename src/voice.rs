use brr::Brr;
use envelope::Envelope;
use envelope::EnvelopePhase;
use bus::RAM;
use util::*;

pub struct Voice {
    pub volume_l: i8,
    pub volume_r: i8,
    pub pitch_lo: u8,
    pub pitch_hi: u8,

    pub key_off : bool,
    pub voice_end: bool,
    pub echo_enable: bool,
    pub noise_enable: bool,
    pub pitch_modulation_enable: bool,

    pub source_number: u8,

    current_sample_value: i16, // read-only register, current sample value after envelope

    samples_since_key_on: u32,

    brr: Brr,
    pub envelope: Envelope
}

impl Voice {
    pub fn new() -> Voice {
        Voice {
            volume_l: 0, volume_r: 0,
            pitch_lo: 0, pitch_hi: 0,

            key_off: false, voice_end: false,
            echo_enable: false, noise_enable: false, pitch_modulation_enable: false,

            source_number: 0,

            current_sample_value: 0,

            samples_since_key_on: 0,

            brr: Brr::new(),
            envelope: Envelope::new()
        }
    }

    pub fn calculate_next_sample(&mut self, ram: &mut RAM) {
        if (self.samples_since_key_on >= 2) && self.key_off {
            // Key off isn't checked until after a couple of samples. If we don't allow this,
            // some notes will not start on some files, because they trigger key onn FIRST and only after do they they clear KOFF.
            self.envelope.adsr_phase = EnvelopePhase::Release; // This check is done every time.
        }

        self.envelope.tick();

        let pitch = self.pitch(); // pitch can be interpreted as '1/4096ths of an integer source waveform sample increment for each output sample

        if self.pitch_modulation_enable {
            panic!("Pitch modulation feature not implemented."); // TODO
        }
        if self.noise_enable {
            panic!("Noise generation feature not implemented."); // TODO
        }

        let sample: i16 = self.brr.next_sample(pitch, ram);
        if self.brr.is_end_reached() {
            self.voice_end = true;
        }
        if self.brr.is_terminate_immediately() {
            self.envelope.adsr_phase = EnvelopePhase::Release;
            self.envelope.current_value = 0;
        }

        let after_envelope = (((sample as i32) * (self.envelope.current_value as i32)) >> 11) as i16; // TODO will this cast do anything unexpected?

        self.current_sample_value = after_envelope;

        self.samples_since_key_on = self.samples_since_key_on.saturating_add(1);
    }

    pub fn current_sample_before_volume_adjustment(&self) -> i16 {
        self.current_sample_value
    }

    pub fn current_sample(&self) -> (i16, i16) {
        (self.current_sample_value, self.current_sample_value).adjust_volume(self.volume_l, self.volume_r)
    }

    fn pitch(&self) -> u16 {
        u16::hi_lo(self.pitch_hi, self.pitch_lo) & 0x3fff
    }

    pub fn key_on(&mut self, start_pointer: u16, loop_pointer: u16) {
        self.voice_end = false;
        self.envelope.restart();
        self.brr.start(start_pointer, loop_pointer);
        self.samples_since_key_on = 0;
    }

    pub fn key_off(&mut self, value: bool) {
        self.key_off = value;
    }

    pub fn debug_info(&self, with_header: bool) -> String {
        use std::ops::Add;
        let header    = format!("L VOL R | PITCH | OFF END ECH NSE MOD | SRCE | PTR  |       ENV      |");
        let values    = format!("{:+03x} {:+03x} |  {:04x} |  {}   {}   {}   {}   {}  |  {:02x}  | {:04x} | {} |",
            self.volume_l, self.volume_r, self.pitch(),
            self.key_off.as_digit(), self.voice_end.as_digit(), self.echo_enable.as_digit(), self.noise_enable.as_digit(), self.pitch_modulation_enable.as_digit(),
            self.source_number, self.brr.current_pointer(), self.envelope.debug_info());

        let loudness_units = Voice::loudness_units(self.volume_l, self.volume_r, self.envelope.current_value);
        let meter = METERS[loudness_units as usize];
        if with_header { header.add("\n").add(&values).add(&meter) } else { values.add(&meter) }
    }

    /// Return a number from 0 to 7 representing the 'loudness' of the channel based on the parameters given.
    /// This is used to visually display the channel level.
    /// The algorithm used is completely arbitrary to give a nice range over the possible input values.
    /// It has no relationship with any scientifically-defined notion of 'loudness'.
    /// Note: envelope_value is expected to be in its usual 11-bit range.
    fn loudness_units(volume_l: i8, volume_r: i8, envelope_value: u16) -> u8 {
        let average_volume: f32 = ((volume_l as f32).abs() + (volume_r as f32).abs()) / 2.0 / (0x80 as f32); //scale of 0.0 .. 1.0
        let linear_loudness: f32 = (envelope_value as f32 / (0x800 as f32)) * average_volume; //scale of 0.0 .. 1.0
        let log_loudness: f32 = (10.0 * linear_loudness.log10()).round();
        let visual_loudness_units: isize = (log_loudness as isize / 2) + 8;
        if visual_loudness_units < 0 { 0 }
        else if visual_loudness_units > 7 { 7 }
        else { visual_loudness_units as u8 }
    }
}

static METERS: [&str; 8] = [
    "       ",
    "|      ",
    "||     ",
    "|||    ",
    "||||   ",
    "|||||  ",
    "|||||| ",
    "|||||||"
];