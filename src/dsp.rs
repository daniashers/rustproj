use clock::Clockable;
use voice::Voice;
use echo::Echo;
use bus::RAM;
use std::cell::RefCell;
use std::rc::Rc;
use util::*;
use std::time::SystemTime;

pub struct Dsp {
    registers: [u8; 128],
    pub register_select: u8,

    pub voices: [Voice; 8],
    echo: Echo,

    pub output_buffer: Vec<(i16, i16)>,

    benchmark_nanos: u64, // used to benchmark how long it takes to generate a sample

    ram: Rc<RefCell<RAM>>
}

impl Dsp {
    pub fn new(ram: Rc<RefCell<RAM>>) -> Dsp {
        Dsp {
            registers: [0u8; 128],
            register_select: 0u8,
            voices: [
                Voice::new(), Voice::new(), Voice::new(), Voice::new(),
                Voice::new(), Voice::new(), Voice::new(), Voice::new()
            ],
            echo: Echo::new(),

            benchmark_nanos: 0,

            output_buffer: vec!(),
            ram: ram
        }
    }

    //
    // Register extractors
    //

    // Convenience function to avoid having to say 'as usize' every time
    #[inline]
    pub fn reg(&self, reg_index: u8) -> u8 { self.registers[reg_index as usize] }

    pub fn master_volume_l(&self)            -> i8  { self.reg(DSPREG::MVOLL) as i8 }
    pub fn master_volume_r(&self)            -> i8  { self.reg(DSPREG::MVOLR) as i8 }
    pub fn echo_volume_l(&self)              -> i8  { self.reg(DSPREG::EVOLL) as i8 }
    pub fn echo_volume_r(&self)              -> i8  { self.reg(DSPREG::EVOLR) as i8 }
    pub fn echo_feedback_amount(&self)       -> i8  { self.reg(DSPREG::EFB) as i8 }
    pub fn directory_address(&self)          -> u16 { u16::hi_lo(self.reg(DSPREG::DIR), 0x00) }
    pub fn echo_buffer_address(&self)        -> u16 { u16::hi_lo(self.reg(DSPREG::ESA), 0x00) }
    pub fn echo_buffer_length_bytes(&self)   -> u16 { ((self.reg(DSPREG::EDL) & 0x0f) as u16) << 11 }
    pub fn echo_buffer_length_samples(&self) -> u16 { ((self.reg(DSPREG::EDL) & 0x0f) as u16) << 9 }

    //
    // Other functions
    //

    // Read the address of the start of the sample data for the given source number.
    pub fn source_start_addr(&self, n: u8) -> u16 {
        let deref_ram: &mut RAM = &mut (*(self.ram.borrow_mut()));
        let pointer_addr = self.directory_address().wrapping_add((n as u16) * 4);
        u16::hi_lo(deref_ram[(pointer_addr + 1) as usize], deref_ram[pointer_addr as usize])
    }

    // Read the address of the looping section of the sample data for the given source number.
    pub fn source_loop_addr(&self, n: u8) -> u16 {
        let deref_ram: &mut RAM = &mut (*(self.ram.borrow_mut()));
        let pointer_addr = self.directory_address().wrapping_add((n as u16) * 4).wrapping_add(2);
        u16::hi_lo(deref_ram[(pointer_addr + 1) as usize], deref_ram[pointer_addr as usize])
    }

    pub fn read_register(&self) -> u8 {
        let reg = self.register_select & 0x7f;
        // registers above 0x7f can be read, but are mirrors of 0-0x7f
        match reg {
            DSPREG::KON  => { 0x00 }, // KON should return zero, the values written get reset all the time.
            DSPREG::KOFF => { u8::make_bitmap(|n| self.voices[n].key_off) }
            DSPREG::ENDX => { u8::make_bitmap(|n| self.voices[n].voice_end) }
            DSPREG::PMON => { u8::make_bitmap(|n| self.voices[n].pitch_modulation_enable) }
            DSPREG::NON  => { u8::make_bitmap(|n| self.voices[n].noise_enable) }
            DSPREG::EON  => { u8::make_bitmap(|n| self.voices[n].echo_enable) }
            DSPREG::V0ENVX => (self.voices[0].envelope.current_value >> 3) as u8, // value is 11 bits unsigned but reg is only 8 bits so we lose the 3 least significant bits
            DSPREG::V1ENVX => (self.voices[1].envelope.current_value >> 3) as u8,
            DSPREG::V2ENVX => (self.voices[2].envelope.current_value >> 3) as u8,
            DSPREG::V3ENVX => (self.voices[3].envelope.current_value >> 3) as u8,
            DSPREG::V4ENVX => (self.voices[4].envelope.current_value >> 3) as u8,
            DSPREG::V5ENVX => (self.voices[5].envelope.current_value >> 3) as u8,
            DSPREG::V6ENVX => (self.voices[6].envelope.current_value >> 3) as u8,
            DSPREG::V7ENVX => (self.voices[7].envelope.current_value >> 3) as u8,
            DSPREG::V0OUTX => (self.voices[0].current_sample_before_volume_adjustment() >> 8) as i8 as u8, // value is 16 bits signed but reg is only 8 bits so we lose the less significant bits
            DSPREG::V1OUTX => (self.voices[1].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V2OUTX => (self.voices[2].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V3OUTX => (self.voices[3].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V4OUTX => (self.voices[4].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V5OUTX => (self.voices[5].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V6OUTX => (self.voices[6].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            DSPREG::V7OUTX => (self.voices[7].current_sample_before_volume_adjustment() >> 8) as i8 as u8,
            _ => { self.registers[reg as usize] }

        }

        // TODO
        // Most values written to the registers, although they are 'passed on' to internal structures,
        // are not really overwritten and so can be read back, unchanged, from the register array itself.
        // Those values which are modified by internal structures, however, e.g. VxENVX and VxOUTX,
        // should be re-fetched fresh from the voices.
    }

    pub fn write_register(&mut self, value: u8) {
        if self.register_select < 0x80 { // registers above 0x7f do not exist
            //println!("DSP write: {:02x} ({}) := {:02x}", self.register_select, REG_NAMES[self.register_select as usize], value);
            self.registers[self.register_select as usize] = value;

            Dsp::voice_nr_affected(self.register_select).map( |voice_nr|
                match self.register_select & 0x0f {
                    0 => { self.voices[voice_nr as usize].volume_l = value as i8; },
                    1 => { self.voices[voice_nr as usize].volume_r = value as i8; },
                    2 => { self.voices[voice_nr as usize].pitch_lo = value; },
                    3 => { self.voices[voice_nr as usize].pitch_hi = value; },
                    4 => { self.voices[voice_nr as usize].source_number = value; },
                    5 => { self.voices[voice_nr as usize].envelope.adsr1 = value; },
                    6 => { self.voices[voice_nr as usize].envelope.adsr2 = value; },
                    7 => { self.voices[voice_nr as usize].envelope.gain = value; },
                    8 => { }  // We don't bother writing these, because the voice overwrites them each sample
                    9 => { }  // We don't bother writing these, because the voice overwrites them each sample
                    _ => { }
                }
            );

            if Dsp::affects_all_voices(self.register_select) {
                match self.register_select {
                    /* Key on */
                    DSPREG::KON => {
                        value.for_each_bit(|n, value| {
                            if value { // if key on = true, we set the pointers correctly before keying on
                                let start_pointer = self.source_start_addr(self.voices[n as usize].source_number);
                                let loop_pointer = self.source_loop_addr(self.voices[n as usize].source_number);
                                self.voices[n as usize].key_on(start_pointer, loop_pointer);
                            }
                        } );
                        self.registers[DSPREG::KON as usize] = 0x00 // Key-on register acts 'on write' but doesn't 'stay' set.
                    }
                    DSPREG::KOFF => { value.for_each_bit(|n, value| self.voices[n as usize].key_off(value) ); }
                    DSPREG::ENDX => { value.for_each_bit(|n, value| self.voices[n as usize].voice_end = false ); } // TODO the actual behaviour is that a write to any bit resets the whole register to zero
                    DSPREG::PMON => { value.for_each_bit(|n, value| self.voices[n as usize].pitch_modulation_enable = value ); }
                    DSPREG::NON  => { value.for_each_bit(|n, value| self.voices[n as usize].noise_enable = value ); }
                    DSPREG::EON  => { value.for_each_bit(|n, value| self.voices[n as usize].echo_enable = value ); }
                    _ => panic!("Not possible")
                }
            }

            if self.register_select == DSPREG::ESA {
                self.echo.buffer_start = u16::hi_lo(value, 0x00);
            }
            if self.register_select == DSPREG::EDL {
                self.echo.sample_capacity = ((value & 0x0f) as u16) << 9;
            }
            // is this a filter coefficient?
            // 0f, 1f, 2f, 3f, 4f, 5f, 6f or 7f
            if (self.register_select & 0x0f == 0x0f) && (self.register_select >> 4 <= 8) {
                let coefficient_number = self.register_select >> 4;
                self.echo.filter_coefficients[coefficient_number as usize] = value as i8;
            }
        }
    }

    fn noise_frequency(&self) -> u16 {
        COUNTER_RATES[(self.registers[DSPREG::FLG as usize] & 0x1f) as usize]
    }

    fn voice_nr_affected(register_nr: u8) -> Option<u8> {
        if (register_nr & 0x0f) > 9 { None } // TODO is this right??? REVIEW THIS WHOLE FUNCTION
        else {
            let voice_nr = (register_nr >> 4) & 0x0f;
            if (voice_nr) > 7 { None } else { Some(voice_nr) }
        }
    }

    fn affects_all_voices(register_nr: u8) -> bool {
        register_nr == DSPREG::KON  ||
        register_nr == DSPREG::KOFF ||
        register_nr == DSPREG::ENDX ||
        register_nr == DSPREG::PMON ||
        register_nr == DSPREG::NON  ||
        register_nr == DSPREG::EON
        // Todo technically FLG also affects all voices but is not a bitfield.
    }

    // The all-important function to call to obtain the next sample.
    fn generate_sample(&mut self) {
        let t0 = SystemTime::now(); // for benchmarking purposes

        let ram: &mut RAM = &mut (*(self.ram.borrow_mut()));

        for voice_nr in 0..8 {
            self.voices[voice_nr].calculate_next_sample(ram);
        }

        let clean: StereoSample = {
            // Mix down all voices
            let downmix: StereoSample = self.voices.iter().fold((0i16, 0i16), |accum, ref voice| {
                accum.saturating_add(voice.current_sample())
            });
            // Adjust the volume
            downmix.adjust_volume(self.master_volume_l(), self.master_volume_r())
        };

        let echo: StereoSample = self.echo.get_filtered_sample(ram).adjust_volume(self.echo_volume_r(), self.echo_volume_r());

        let post_fx: StereoSample = clean.saturating_add(echo);

        // Output the sample!
        self.output_buffer.push(post_fx); // TODO! if mute flag is set, output silence instead!

        let new_echo_sample: StereoSample = {
            let pre_feedback = self.voices.iter().fold((0i16, 0i16), |accum, voice| {
                if voice.echo_enable {
                    accum.saturating_add(voice.current_sample())
                } else { accum }
            });
            pre_feedback.saturating_add(self.echo.get_filtered_sample(ram).adjust_volume(self.echo_feedback_amount(), self.echo_feedback_amount()))
        };

        // Write the new echo sample to the echo processor.
        self.echo.put_sample(ram, new_echo_sample);

        // write benchmark
        let time_taken = SystemTime::now().duration_since(t0).unwrap().subsec_nanos(); // TODO if it took more than 1 sec, this would be wrong! subsec_nanos returns ONLY the fractional part of the duration!
        self.benchmark_nanos += time_taken as u64;
    }

    pub fn debug_info(&self) -> String {
        use std::ops::Add;

        let fir_string = format!("{:2x}{:2x}{:2x}{:2x}{:2x}{:2x}{:2x}{:2x}",
                                 self.reg(DSPREG::FFC0), self.reg(DSPREG::FFC1), self.reg(DSPREG::FFC2), self.reg(DSPREG::FFC3),
                                 self.reg(DSPREG::FFC4), self.reg(DSPREG::FFC5), self.reg(DSPREG::FFC6), self.reg(DSPREG::FFC7));
        //      let echo_line = format!("+ll +rr |  n/a  | FC=xxxxxxxxxxxxxxxx | ECHO | xxxx | DLYxxxms F/B+xx |");
        let header = "L MASTER R | L  ECHO  R | F/B |       FFC        |    BUF    |  DLY  |\n";
        let info = format!(" {:+03x}  {:+03x}  |  {:+03x}  {:+03x}  | {:+03x} | {} | {:04x}-{:04x} | {:3}ms |",
                self.master_volume_l(), self.master_volume_r(),
                self.echo_volume_l(), self.echo_volume_r(), self.echo_feedback_amount(), &fir_string,
                self.echo.buffer_start, self.echo.buffer_end(), self.echo_buffer_length_samples() / 32);
        header.to_string().add(&info)
    }

    pub fn read_and_reset_benchmark_nanos(&mut self) -> u64 {
        let total_nanos = self.benchmark_nanos;
        self.benchmark_nanos = 0;
        total_nanos
    }
}

impl Clockable for Dsp {
    /// In the real world the DSP should be clocked at about 24 MHz if we wanted to reproduce all
    /// the low-level timings. But in our case we're happy to call it just once every sample.
    /// So, this method should be called at a rate of 32.0 kHz.
    fn tick(&mut self) {
        self.generate_sample();
    }
}


pub const COUNTER_RATES: [u16; 32] = [
    0, // should really be infinite, but we will interpret zero as such
    2048, 1536, 1280, 1024, 768,  640,  512,
    384,  320,  256,  192,  160,  128,  96,   80,
    64,   48,   40,   32,   24,   20,   16,   12,
    10,   8,    6,    5,    4,    3,    2,    1
];

pub struct DSPREG {}

#[allow(unused)]
impl DSPREG {
    pub const V0VOLL: u8 = 0x00;
    pub const V1VOLL: u8 = 0x10;
    pub const V2VOLL: u8 = 0x20;
    pub const V3VOLL: u8 = 0x30;
    pub const V4VOLL: u8 = 0x40;
    pub const V5VOLL: u8 = 0x50;
    pub const V6VOLL: u8 = 0x60;
    pub const V7VOLL: u8 = 0x70;
    pub const V0VOLR: u8 = 0x01;
    pub const V1VOLR: u8 = 0x11;
    pub const V2VOLR: u8 = 0x21;
    pub const V3VOLR: u8 = 0x31;
    pub const V4VOLR: u8 = 0x41;
    pub const V5VOLR: u8 = 0x51;
    pub const V6VOLR: u8 = 0x61;
    pub const V7VOLR: u8 = 0x71;
    pub const V0PITCHL: u8 = 0x02;
    pub const V1PITCHL: u8 = 0x12;
    pub const V2PITCHL: u8 = 0x22;
    pub const V3PITCHL: u8 = 0x32;
    pub const V4PITCHL: u8 = 0x42;
    pub const V5PITCHL: u8 = 0x52;
    pub const V6PITCHL: u8 = 0x62;
    pub const V7PITCHL: u8 = 0x72;
    pub const V0PITCHH: u8 = 0x03;
    pub const V1PITCHH: u8 = 0x13;
    pub const V2PITCHH: u8 = 0x23;
    pub const V3PITCHH: u8 = 0x33;
    pub const V4PITCHH: u8 = 0x43;
    pub const V5PITCHH: u8 = 0x53;
    pub const V6PITCHH: u8 = 0x63;
    pub const V7PITCHH: u8 = 0x73;
    pub const V0SRCN: u8 = 0x04;
    pub const V1SRCN: u8 = 0x14;
    pub const V2SRCN: u8 = 0x24;
    pub const V3SRCN: u8 = 0x34;
    pub const V4SRCN: u8 = 0x44;
    pub const V5SRCN: u8 = 0x54;
    pub const V6SRCN: u8 = 0x64;
    pub const V7SRCN: u8 = 0x74;
    pub const V0ADSR1: u8 = 0x05;
    pub const V1ADSR1: u8 = 0x15;
    pub const V2ADSR1: u8 = 0x25;
    pub const V3ADSR1: u8 = 0x35;
    pub const V4ADSR1: u8 = 0x45;
    pub const V5ADSR1: u8 = 0x55;
    pub const V6ADSR1: u8 = 0x65;
    pub const V7ADSR1: u8 = 0x75;
    pub const V0ADSR2: u8 = 0x06;
    pub const V1ADSR2: u8 = 0x16;
    pub const V2ADSR2: u8 = 0x26;
    pub const V3ADSR2: u8 = 0x36;
    pub const V4ADSR2: u8 = 0x46;
    pub const V5ADSR2: u8 = 0x56;
    pub const V6ADSR2: u8 = 0x66;
    pub const V7ADSR2: u8 = 0x76;
    pub const V0GAIN: u8 = 0x07;
    pub const V1GAIN: u8 = 0x17;
    pub const V2GAIN: u8 = 0x27;
    pub const V3GAIN: u8 = 0x37;
    pub const V4GAIN: u8 = 0x47;
    pub const V5GAIN: u8 = 0x57;
    pub const V6GAIN: u8 = 0x67;
    pub const V7GAIN: u8 = 0x77;
    pub const V0ENVX: u8 = 0x08;
    pub const V1ENVX: u8 = 0x18;
    pub const V2ENVX: u8 = 0x28;
    pub const V3ENVX: u8 = 0x38;
    pub const V4ENVX: u8 = 0x48;
    pub const V5ENVX: u8 = 0x58;
    pub const V6ENVX: u8 = 0x68;
    pub const V7ENVX: u8 = 0x78;
    pub const V0OUTX: u8 = 0x09;
    pub const V1OUTX: u8 = 0x19;
    pub const V2OUTX: u8 = 0x29;
    pub const V3OUTX: u8 = 0x39;
    pub const V4OUTX: u8 = 0x49;
    pub const V5OUTX: u8 = 0x59;
    pub const V6OUTX: u8 = 0x69;
    pub const V7OUTX: u8 = 0x79;
    pub const MVOLL: u8 = 0x0c;
    pub const MVOLR: u8 = 0x1c;
    pub const EVOLL: u8 = 0x2c;
    pub const EVOLR: u8 = 0x3c;
    pub const KON: u8 = 0x4c;
    pub const KOFF: u8 = 0x5c;
    pub const FLG: u8 = 0x6c;
    pub const ENDX: u8 = 0x7c;
    pub const EFB: u8 = 0x0d;
    pub const PMON: u8 = 0x2d;
    pub const NON: u8 = 0x3d;
    pub const EON: u8 = 0x4d;
    pub const DIR: u8 = 0x5d;
    pub const ESA: u8 = 0x6d;
    pub const EDL: u8 = 0x7d;
    pub const FFC0: u8 = 0x0f;
    pub const FFC1: u8 = 0x1f;
    pub const FFC2: u8 = 0x2f;
    pub const FFC3: u8 = 0x3f;
    pub const FFC4: u8 = 0x4f;
    pub const FFC5: u8 = 0x5f;
    pub const FFC6: u8 = 0x6f;
    pub const FFC7: u8 = 0x7f;

}

#[allow(unused)]
const REG_NAMES: [&str; 128] = [
    "V0VOLL",   "V0VOLR",   "V0PITCHL", "V0PITCHH", "V0SRCN",   "V0ADSR1",  "V0ADSR2",  "V0GAIN",
    "V0ENVX",   "V0OUTX",   "?",        "?",        "MVOLL",    "EFB",      "?",        "FFC0",
    "V1VOLL",   "V1VOLR",   "V1PITCHL", "V1PITCHH", "V1SRCN",   "V1ADSR1",  "V1ADSR2",  "V1GAIN",
    "V1ENVX",   "V1OUTX",   "?",        "?",        "MVOLR",    "?",        "?",        "FFC1",
    "V2VOLL",   "V2VOLR",   "V2PITCHL", "V2PITCHH", "V2SRCN",   "V2ADSR1",  "V2ADSR2",  "V2GAIN",
    "V2ENVX",   "V2OUTX",   "?",        "?",        "EVOLL",    "PMON",     "?",        "FFC2",
    "V3VOLL",   "V3VOLR",   "V3PITCHL", "V3PITCHH", "V3SRCN",   "V3ADSR1",  "V3ADSR2",  "V3GAIN",
    "V3ENVX",   "V3OUTX",   "?",        "?",        "EVOLR",    "NON",      "?",        "FFC3",
    "V4VOLL",   "V4VOLR",   "V4PITCHL", "V4PITCHH", "V4SRCN",   "V4ADSR1",  "V4ADSR2",  "V4GAIN",
    "V4ENVX",   "V4OUTX",   "?",        "?",        "KON",      "EON",      "?",        "FFC4",
    "V5VOLL",   "V5VOLR",   "V5PITCHL", "V5PITCHH", "V5SRCN",   "V5ADSR1",  "V5ADSR2",  "V5GAIN",
    "V5ENVX",   "V5OUTX",   "?",        "?",        "KOFF",     "DIR",      "?",        "FFC5",
    "V6VOLL",   "V6VOLR",   "V6PITCHL", "V6PITCHH", "V6SRCN",   "V6ADSR1",  "V6ADSR2",  "V6GAIN",
    "V6ENVX",   "V6OUTX",   "?",        "?",        "FLG",      "ESA",      "?",        "FFC6",
    "V7VOLL",   "V7VOLR",   "V7PITCHL", "V7PITCHH", "V7SRCN",   "V7ADSR1",  "V7ADSR2",  "V7GAIN",
    "V7ENVX",   "V7OUTX",   "?",        "?",        "ENDX",     "EDL",      "?",        "FFC7"
];
