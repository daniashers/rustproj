mod util;
mod load;
mod audioout;
mod cpu;
mod op;
mod bus;
mod timer;
mod clock;
mod rom;
mod dsp;
mod voice;
mod brr;
mod envelope;
mod echo;
mod upsample;

use std::env as environment;

use cpu::Cpu;
use bus::*;
use rom::ROM;
use dsp::Dsp;
use std::time::SystemTime;
use load::LoadedFile;
use clock::Clockable;
use timer::Timer;
use std::cell::Cell;
use std::cell::RefCell;
use std::rc::Rc;
use std::clone::Clone;
use bus::RAM;
use upsample::Upsample;

fn main() {

    let args: Vec<String> = environment::args().collect();
    let filename = args[1].clone();

    let loaded_file = LoadedFile::from_disk(filename);
    println!("{}: {} ({})", loaded_file.composer(), loaded_file.song_title(), loaded_file.game());

    let ram: Rc<RefCell<RAM>> = Rc::new(RefCell::new(loaded_file.ram()));
    let dsp: Rc<RefCell<Dsp>> = Rc::new(RefCell::new(Dsp::new(ram.clone())));
    let bus: Bus =
        Bus {
            ram: ram,
            rom: &ROM,
            dsp: dsp.clone(),
            timer0: RefCell::new(Timer::new()),
            timer1: RefCell::new(Timer::new()),
            timer2: RefCell::new(Timer::new()),
            internal_counter: Cell::new(0)
        };

    loaded_file.set_dsp_state(&mut (*((*dsp).borrow_mut()))); // TODO this line is horrendous, there must be a better way.
    let cpu_state = loaded_file.cpu_state();
    let mut cpu: Cpu<Bus> = Cpu::new(&bus, cpu_state);
    loaded_file.reinit_system_registers(&bus);

    let mut upsampler = Upsample::new();

    let generate_n_samples = |n: usize| {

        while upsampler.samples_available() < n {
            let t0 = SystemTime::now();

            let n = 320; // Run it for long enough to generate 320 samples.

            for _ in 0..n {
                for _ in 0..32 { // a sample is generated every 32 CPU cycles
                    cpu.tick();
                    bus.tick_timers();
                }
                (*dsp).borrow_mut().tick();
            }

            const ACTUAL_NANOSECONDS_PER_SAMPLE: usize = 31250; // corresponding to 32000 Hz

            let time_taken_per_sample: u64 = SystemTime::now().duration_since(t0).unwrap().subsec_nanos() as u64 / n; // TODO this is wrong. subsec_nanos returns ONLY the fractional part of the duration!
            let multiplier = ACTUAL_NANOSECONDS_PER_SAMPLE as f32 / time_taken_per_sample as f32;
            let effective_frequency = 1.024 * multiplier;

            let mut borrowed_dsp = (*dsp).borrow_mut();
            let dsp_time_taken_per_sample = borrowed_dsp.read_and_reset_benchmark_nanos() / n;
            let core_time_taken_per_sample = time_taken_per_sample - dsp_time_taken_per_sample / n;
            let dsp_time_ratio = dsp_time_taken_per_sample as f32 / ACTUAL_NANOSECONDS_PER_SAMPLE as f32;
            let core_time_ratio = core_time_taken_per_sample as f32 / ACTUAL_NANOSECONDS_PER_SAMPLE as f32;

            upsampler.feed(&borrowed_dsp.output_buffer[..]);
            borrowed_dsp.output_buffer.clear();

            println!("{}", borrowed_dsp.debug_info());
            //println!("T0: {} T1: {} T2: {}", bus.timer_0_period_nanos().format_nanos(), bus.timer_1_period_nanos().format_nanos(), bus.timer_2_period_nanos().format_nanos());
            println!("{}", borrowed_dsp.voices[0].debug_info(true));
            println!("{}", borrowed_dsp.voices[1].debug_info(false));
            println!("{}", borrowed_dsp.voices[2].debug_info(false));
            println!("{}", borrowed_dsp.voices[3].debug_info(false));
            println!("{}", borrowed_dsp.voices[4].debug_info(false));
            println!("{}", borrowed_dsp.voices[5].debug_info(false));
            println!("{}", borrowed_dsp.voices[6].debug_info(false));
            println!("{}", borrowed_dsp.voices[7].debug_info(false));
            println!("{:5.1} MHz, core {:5.1}%, DSP {:5.1}%", effective_frequency, core_time_ratio * 100.0, dsp_time_ratio * 100.0);
            print!("\x1b[12A"); // ANSI escape code to move the cursor up n lines
        }

        upsampler.gimme_n_samples(n)
    };

    audioout::start_output_loop(generate_n_samples);

}

