use op::Instruction;
use clock::Clockable;
use bus::Addressable;
use std::fmt;
use std::ops::Add;
use util::*;
use std::cell::RefCell;

pub struct Cpu<'a, A: 'a + Addressable + ?Sized> {
    bus_ref: &'a A,
    state: RefCell<CpuState>
}

#[derive(Clone)]
pub struct CpuState {
    pub pc: u16,
    pub a: u8,
    pub x: u8,
    pub y: u8,
    pub psw: u8, // NVPBHIZC
    pub sp: u8,

    pub busy_cycles: u8,
    pub halted: bool
}

impl CpuState {
    pub fn new() -> CpuState {
        CpuState { pc: 0, a: 0, x: 0, y: 0, psw: 0, sp: 0, busy_cycles: 0, halted: false }
    }

    pub fn n(&self) -> bool { self.psw.test(7) }
    pub fn v(&self) -> bool { self.psw.test(6) }
    pub fn p(&self) -> bool { self.psw.test(5) }
    pub fn b(&self) -> bool { self.psw.test(4) }
    pub fn h(&self) -> bool { self.psw.test(3) }
    pub fn i(&self) -> bool { self.psw.test(2) }
    pub fn z(&self) -> bool { self.psw.test(1) }
    pub fn c(&self) -> bool { self.psw.test(0) }

    pub fn set_n(&mut self, value: bool) { self.psw = self.psw.modify_bit(7, value) }
    pub fn set_v(&mut self, value: bool) { self.psw = self.psw.modify_bit(6, value) }
    pub fn set_p(&mut self, value: bool) { self.psw = self.psw.modify_bit(5, value) }
    pub fn set_b(&mut self, value: bool) { self.psw = self.psw.modify_bit(4, value) }
    pub fn set_h(&mut self, value: bool) { self.psw = self.psw.modify_bit(3, value) }
    pub fn set_i(&mut self, value: bool) { self.psw = self.psw.modify_bit(2, value) }
    pub fn set_z(&mut self, value: bool) { self.psw = self.psw.modify_bit(1, value) }
    pub fn set_c(&mut self, value: bool) { self.psw = self.psw.modify_bit(0, value) }

    pub fn ya(&self) -> u16 {
        u16::hi_lo(self.y, self.a)
    }
    pub fn set_ya(&mut self, value: u16) {
        self.a = value.lo();
        self.y = value.hi();
    }

    // Applies the Negative and Zero flags based on the passed value.
    fn apply_nz(&mut self, value: u8) {
        self.set_n(value.test(7));
        self.set_z(value == 0);
    }

    fn adc_and_set_flags(&mut self, value1: u8, value2: u8) -> u8 {
        let old_carry: u8 = if self.c() { 1 } else { 0 };
        let (result0, carry0) = value1.overflowing_add(value2);
        let (result, carry1) = result0.overflowing_add(old_carry);
        let carry = carry0 | carry1;
        let half_carry = (((value1 & 0x0f) + (value2 & 0x0f) + old_carry) & 0x10) != 0;
        let overflow = ((value1 & 0x80) == (value2 & 0x80)) && ((value1 & 0x80) != (result & 0x80)); // any corner cases with the carry here?
        self.apply_nz(result);
        self.set_c(carry);
        self.set_h(half_carry);
        self.set_v(overflow);
        result
    }

    fn sbc_and_set_flags(&mut self, value1: u8, value2: u8) -> u8 {
        let old_borrow: u8 = if self.c() { 0 } else { 1 };
        let (result0, borrow0) = value1.overflowing_sub(value2);
        let (result, borrow1) = result0.overflowing_sub(old_borrow);
        let borrow = borrow0 | borrow1;
        let half_borrow = (value1 & 0x0f) < ((value2 & 0x0f) + old_borrow);
        let overflow = ((value1 & 0x80) != (value2 & 0x80)) && ((value1 & 0x80) != (result & 0x80));
        self.apply_nz(result);
        self.set_c(!borrow);
        self.set_h(!half_borrow);
        self.set_v(overflow);
        result
    }

    fn cmp_and_set_flags(&mut self, value1: u8, value2: u8) {
        let (result, borrow) = value1.overflowing_sub(value2);
        self.apply_nz(result);
        self.set_c(!borrow);
    }

    // Converts a direct-page location into an absolute address
    fn dp_addr(&self, low_byte: u8) -> u16 {
        if self.p() { 0x0100 | (low_byte as u16) } else { low_byte as u16 }
    }

}

impl fmt::Display for CpuState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let psw = {
            let n = if self.n() { "N" } else { "-" };
            let v = if self.v() { "V" } else { "-" };
            let p = if self.p() { "P" } else { "-" };
            let b = if self.b() { "B" } else { "-" };
            let h = if self.h() { "H" } else { "-" };
            let i = if self.i() { "I" } else { "-" };
            let z = if self.z() { "Z" } else { "-" };
            let c = if self.c() { "C" } else { "-" };
            "".to_string().add(n).add(v).add(p).add(b).add(h).add(i).add(z).add(c)
        };
        write!(f, "CPU A={:02x} X={:02x} Y={:02x} PC={:04x} SP={:02x} S={}", self.a, self.x, self.y, self.pc, self.sp, psw)
    }
}

impl<'a, A: Addressable + ?Sized> Cpu<'a, A> {

    pub fn new(bus_ref: &'a A, state: CpuState) -> Cpu<A> {
        Cpu { bus_ref: bus_ref, state: RefCell::new(state) }
    }

    // Reads a 16-bit word from the specified address
    fn read16(state: &CpuState, bus: &A, addr: u16) -> u16 {
        let low = bus.read(addr);
        let high = bus.read(addr.wrapping_add(1));
        u16::hi_lo(high, low)
    }

    // Reads a byte from the specified direct-page location
    fn dp_read(state: &CpuState, bus: &A, dp_addr: u8) -> u8 { let addr = state.dp_addr(dp_addr); bus.read(addr) }

    // Writes a byte to the specified direct-page location
    fn dp_write(state: &CpuState, bus: &A, dp_addr: u8, value: u8) { let addr = state.dp_addr(dp_addr); bus.write(addr, value); }

    // Reads a 16-bit word from the specified direct-page location
    fn dp_read16(state: &CpuState, bus: &A, dp_addr: u8) -> u16 {
        let low = Cpu::dp_read(state, bus, dp_addr);
        let high = Cpu::dp_read(state, bus, dp_addr.wrapping_add(1));
        u16::hi_lo(high, low)
    }

    // Writes a 16-bit word to the specified direct-page location
    fn dp_write16(state: &CpuState, bus: &A, dp_addr: u8, value: u16) {
        Cpu::dp_write(state, bus, dp_addr, value.lo());
        Cpu::dp_write(state, bus, dp_addr.wrapping_add(1), value.hi());
    }

    // Fetches a byte from under the program counter and increments it.
    pub fn fetch(state: &mut CpuState, bus: &A) -> u8 {
        let value = bus.read(state.pc);
        state.pc = state.pc.wrapping_add(1);
        value
    }

    // Fetches a 16-bit little endian word from under the program counter and increments it.
    fn fetch16(state: &mut CpuState, bus: &A) -> u16 {
        let lo = Cpu::fetch(state, bus);
        let hi = Cpu::fetch(state, bus);
        u16::hi_lo(hi, lo)
    }

    fn push(state: &mut CpuState, bus: &A, value: u8) {
        bus.write(u16::hi_lo(0x01, state.sp), value);
        state.sp = state.sp.wrapping_sub(1);
    }

    fn pushw(state: &mut CpuState, bus: &A, value: u16) {
        Cpu::push(state, bus, value.hi());
        Cpu::push(state, bus, value.lo());
    }

    fn popw(state: &mut CpuState, bus: &A) -> u16 {
        let lo = Cpu::pop(state, bus);
        let hi = Cpu::pop(state, bus);
        u16::hi_lo(hi, lo)
    }

    fn pop(state: &mut CpuState, bus: &A) -> u8 {
        state.sp = state.sp.wrapping_add(1);
        bus.read(u16::hi_lo(0x01, state.sp))
    }

    fn call(state: &mut CpuState, bus: &A, addr: u16) {
        let pc = state.pc;
        Cpu::pushw(state, bus, pc);
        state.pc = addr;
    }

    /// Convenience function to reduce repetition in execution block
    /// Test bit and branch if clear (direct page value)
    fn bbc(state: &mut CpuState, bus: &A, bit_no: u8) {
        let loc = Cpu::fetch(state, bus);
        let amount = Cpu::fetch(state, bus) as i8;
        let value = Cpu::dp_read(state, bus, loc);
        if !value.test(bit_no) { state.pc = state.pc.signed_add(amount); state.busy_cycles += 7; } else { state.busy_cycles += 5; }
    }

    /// Convenience function to reduce repetition in execution block
    /// Test bit and branch if set (direct page value)
    fn bbs(state: &mut CpuState, bus: &A, bit_no: u8) {
        let loc = Cpu::fetch(state, bus);
        let amount = Cpu::fetch(state, bus) as i8;
        let value = Cpu::dp_read(state, bus, loc);
        if value.test(bit_no) { state.pc = state.pc.signed_add(amount); state.busy_cycles += 7; } else { state.busy_cycles += 5; }
    }

    /// Convenience function to reduce repetition in execution block
    /// Set bit in direct page value
    fn setn(state: &mut CpuState, bus: &A, bit_no: u8) {
        let loc = Cpu::fetch(state, bus);
        let result = Cpu::dp_read(state, bus, loc).modify_bit(bit_no, true);
        Cpu::dp_write(state, bus, loc, result);
        state.busy_cycles += 4;
    }
    /// Convenience function to reduce repetition in execution block
    /// Clear bit in direct page value
    fn clrn(state: &mut CpuState, bus: &A, bit_no: u8) {
        let loc = Cpu::fetch(state, bus);
        let result = Cpu::dp_read(state, bus, loc).modify_bit(bit_no, false);
        Cpu::dp_write(state, bus, loc, result);
        state.busy_cycles += 4;
    }


    pub fn execute(state: &mut CpuState, bus: &A, instruction: &Instruction) {

        macro_rules! mov {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value: u8 = $load_fn!();
                $store_fn!(value);
                state.apply_nz(value);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! mov_st {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value: u8 = $load_fn!();
                $store_fn!(value);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! adc {
            ($load_fn1:ident, $load_fn2:ident, $store_fn:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                let result = state.adc_and_set_flags(left_value, right_value);
                $store_fn!(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! sbc {
            ($load_fn1:ident, $load_fn2:ident, $store_fn:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                let result = state.sbc_and_set_flags(left_value, right_value);
                $store_fn!(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! cmp {
            ($load_fn1:ident, $load_fn2:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                state.cmp_and_set_flags(left_value, right_value);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! and {
            ($load_fn1:ident, $load_fn2:ident, $store_fn:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                let result = left_value & right_value;
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! or {
            ($load_fn1:ident, $load_fn2:ident, $store_fn:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                let result = left_value | right_value;
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! eor {
            ($load_fn1:ident, $load_fn2:ident, $store_fn:ident, $cycles:expr) => {{
                let right_value = $load_fn2!(); // operands such as immediate and dp locations are stored in reverse order in memory
                let left_value = $load_fn1!();  // so we read (2) first and (1) second
                let result = left_value ^ right_value;
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! inc {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                let result = value.wrapping_add(1);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! dec {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                let result = value.wrapping_sub(1);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! asl {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                state.set_c(value.msb());
                let result = value.wrapping_shl(1);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! lsr {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                state.set_c(value.lsb());
                let result = value.wrapping_shr(1);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! rol {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                let old_carry = state.c();
                state.set_c(value.msb());
                let result = value.wrapping_shl(1).modify_lsb(old_carry);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! ror {
            ($load_fn:ident, $store_fn:ident, $cycles:expr) => {{
                let value = $load_fn!();
                let old_carry = state.c();
                state.set_c(value.lsb());
                let result = value.wrapping_shr(1).modify_msb(old_carry);
                $store_fn!(result);
                state.apply_nz(result);
                state.busy_cycles += $cycles;
            }}
        }

        macro_rules! branch_if {
            ($condition:expr) => {{
                let amount = Cpu::fetch(state, bus) as i8;
                if $condition { state.pc = state.pc.signed_add(amount); state.busy_cycles += 4; } else { state.busy_cycles += 2; }
            }}
        }

        macro_rules! read_a { () => { state.a } }
        macro_rules! read_x { () => { state.x } }
        macro_rules! read_y { () => { state.y } }

        macro_rules! write_a { ($value:expr) => { state.a = $value; } }
        macro_rules! write_x { ($value:expr) => { state.x = $value; } }
        macro_rules! write_y { ($value:expr) => { state.y = $value; } }

        macro_rules! read_imm { () => { Cpu::fetch(state, bus) } }

        macro_rules! read_indx  { ()            => { Cpu::dp_read(state, bus, state.x) } }
        macro_rules! write_indx { ($value:expr) => { Cpu::dp_write(state, bus, state.x, $value) } }

        macro_rules! read_indy  { ()            => { Cpu::dp_read(state, bus, state.y) } }
        macro_rules! write_indy { ($value:expr) => { Cpu::dp_write(state, bus, state.y, $value) } }

        macro_rules! read_indxinc {
            () => {{
                let value = Cpu::dp_read(state, bus, state.x);
                state.x = state.x.wrapping_add(1);
                value
            }}
        }
        macro_rules! write_indxinc {
            ($value:expr) => {{
                Cpu::dp_write(state, bus, state.x, $value);
                state.x = state.x.wrapping_add(1);
            }}
        }

        macro_rules! read_dp { () => { { let loc = Cpu::fetch(state, bus); Cpu::dp_read(state, bus, loc) } } }
        macro_rules! peek_dp { () => { { let loc = bus.read(state.pc); Cpu::dp_read(state, bus, loc) } } } // read the dp location without advancing PC!
        macro_rules! write_dp { ($value:expr) => { { let loc = Cpu::fetch(state, bus); Cpu::dp_write(state, bus, loc, $value); } } }

        macro_rules! read_dpidxx { () => { { let loc = Cpu::fetch(state, bus); Cpu::dp_read(state, bus, loc.wrapping_add(state.x)) } } }
        macro_rules! peek_dpidxx { () => { { let loc = bus.read(state.pc); Cpu::dp_read(state, bus, loc.wrapping_add(state.x)) } } }
        macro_rules! write_dpidxx { ($value:expr) => { { let loc = Cpu::fetch(state, bus); Cpu::dp_write(state, bus, loc.wrapping_add(state.x), $value); } } }

        macro_rules! read_dpidxy { () => { { let loc = Cpu::fetch(state, bus); Cpu::dp_read(state, bus, loc.wrapping_add(state.y)) } } }
        macro_rules! peek_dpidxy { () => { { let loc = bus.read(state.pc); Cpu::dp_read(state, bus, loc.wrapping_add(state.y)) } } }
        macro_rules! write_dpidxy { ($value:expr) => { { let loc = Cpu::fetch(state, bus); Cpu::dp_write(state, bus, loc.wrapping_add(state.y), $value); } } }

        macro_rules! read_abs { () => { { let addr = Cpu::fetch16(state, bus); bus.read(addr) } } }
        macro_rules! peek_abs { () => { { let addr = Cpu::read16(state, bus, state.pc); bus.read(addr) } } } // read the value without advancing PC
        macro_rules! write_abs { ($value:expr) => { { let addr = Cpu::fetch16(state, bus); bus.write(addr, $value); } } }

        macro_rules! read_absidxx { () => { { let addr = Cpu::fetch16(state, bus); bus.read(addr.wrapping_add(state.x as u16)) } } }
        macro_rules! write_absidxx { ($value:expr) => { { let addr = Cpu::fetch16(state, bus); bus.write(addr.wrapping_add(state.x as u16), $value); } } }

        macro_rules! read_absidxy { () => { { let addr = Cpu::fetch16(state, bus); bus.read(addr.wrapping_add(state.y as u16)) } } }
        macro_rules! write_absidxy { ($value:expr) => { { let addr = Cpu::fetch16(state, bus); bus.write(addr.wrapping_add(state.y as u16), $value); } } }

        macro_rules! read_idxindx {
            () => {{
                let base_addr = Cpu::fetch(state, bus);
                let final_addr = Cpu::dp_read16(state, bus, base_addr.wrapping_add(state.x));
                bus.read(final_addr)
            }}
        }
        macro_rules! write_idxindx {
            ($value:expr) => {{
                let base_addr = Cpu::fetch(state, bus);
                let final_addr = Cpu::dp_read16(state, bus, base_addr.wrapping_add(state.x));
                bus.write(final_addr, $value)
            }}
        }

        macro_rules! read_indidxy {
            () => {{
                let base_addr = Cpu::fetch(state, bus);
                let final_addr = Cpu::dp_read16(state, bus, base_addr).wrapping_add(state.y as u16);
                bus.read(final_addr)
            }}
        }
        macro_rules! write_indidxy {
            ($value:expr) => {{
                let base_addr = Cpu::fetch(state, bus);
                let final_addr = Cpu::dp_read16(state, bus, base_addr).wrapping_add(state.y as u16);
                bus.write(final_addr, $value)
            }}
        }

        use op::Instruction::*;
        match *instruction {

            // Loads

            Mov_A_Imm        => { mov!(read_imm,     write_a, 2); }
            Mov_A_IndX       => { mov!(read_indx,    write_a, 3); }
            Mov_A_IndXInc    => { mov!(read_indxinc, write_a, 4); }
            Mov_A_Dp         => { mov!(read_dp,      write_a, 3); }
            Mov_A_DpIdxX     => { mov!(read_dpidxx,  write_a, 4); }
            Mov_A_Abs        => { mov!(read_abs,     write_a, 4); }
            Mov_A_AbsIdxX    => { mov!(read_absidxx, write_a, 5); }
            Mov_A_AbsIdxY    => { mov!(read_absidxy, write_a, 5); }
            Mov_A_Ind_DpIdxX => { mov!(read_idxindx, write_a, 6); }
            Mov_A_IndDp_IdxY => { mov!(read_indidxy, write_a, 6); }
            Mov_X_Imm        => { mov!(read_imm,     write_x, 2); }
            Mov_X_Dp         => { mov!(read_dp,      write_x, 3); }
            Mov_X_DpIdxY     => { mov!(read_dpidxy,  write_x, 4); }
            Mov_X_Abs        => { mov!(read_abs,     write_x, 4); }
            Mov_Y_Imm        => { mov!(read_imm,     write_y, 2); }
            Mov_Y_Dp         => { mov!(read_dp,      write_y, 3); }
            Mov_Y_DpIdxX     => { mov!(read_dpidxx,  write_y, 4); }
            Mov_Y_Abs        => { mov!(read_abs,     write_y, 4); }

            // Stores

            Mov_IndX_A       => { mov_st!(read_a, write_indx,    4); }
            Mov_IndXInc_A    => { mov_st!(read_a, write_indxinc, 4); }
            Mov_Dp_A         => { mov_st!(read_a, write_dp,      4); }
            Mov_DpIdxX_A     => { mov_st!(read_a, write_dpidxx,  5); }
            Mov_Abs_A        => { mov_st!(read_a, write_abs,     5); }
            Mov_AbsIdxX_A    => { mov_st!(read_a, write_absidxx, 6); }
            Mov_AbsIdxY_A    => { mov_st!(read_a, write_absidxy, 6); }
            Mov_Ind_DpIdxX_A => { mov_st!(read_a, write_idxindx, 7); }
            Mov_IndDp_IdxY_A => { mov_st!(read_a, write_indidxy, 7); }
            Mov_Dp_X         => { mov_st!(read_x, write_dp,      4); }
            Mov_DpIdxY_X     => { mov_st!(read_x, write_dpidxy,  5); }
            Mov_Abs_X        => { mov_st!(read_x, write_abs,     5); }
            Mov_Dp_Y         => { mov_st!(read_y, write_dp,      4); }
            Mov_DpIdxX_Y     => { mov_st!(read_y, write_dpidxx,  5); }
            Mov_Abs_Y        => { mov_st!(read_y, write_abs,     5); }

            Mov_A_X => {
                let value = state.x;
                state.a = value;
                state.apply_nz(value);
                state.busy_cycles += 2;
            }
            Mov_A_Y => {
                let value = state.y;
                state.a = value;
                state.apply_nz(value);
                state.busy_cycles += 2;
            }
            Mov_X_A => {
                let value = state.a;
                state.x = value;
                state.apply_nz(value);
                state.busy_cycles += 2;
            }
            Mov_Y_A => {
                let value = state.a;
                state.y = value;
                state.apply_nz(value);
                state.busy_cycles += 2;
            }
            Mov_X_SP => {
                let value = state.sp;
                state.x = value;
                state.apply_nz(value);
                state.busy_cycles += 2;
            }
            Mov_SP_X => {
                state.sp = state.x;
                state.busy_cycles += 2;
            }
            Mov_Dp_Dp => {
                let src_dp_loc = Cpu::fetch(state, bus);
                let dest_dp_loc = Cpu::fetch(state, bus);
                let dest_addr = state.dp_addr(dest_dp_loc);
                let src_addr = state.dp_addr(src_dp_loc);
                let value = bus.read(src_addr);
                bus.write(dest_addr, value);
                state.busy_cycles += 5;
            }
            Mov_Dp_Imm => {
                let value = Cpu::fetch(state, bus);
                let dest_dp_loc = Cpu::fetch(state, bus);
                let dest_addr = state.dp_addr(dest_dp_loc);
                bus.write(dest_addr, value);
                state.busy_cycles += 5;
            }

            // Arithmetic

            Adc_A_Imm        => { adc!(read_a,    read_imm,     write_a,    2); }
            Adc_A_IndX       => { adc!(read_a,    read_indx,    write_a,    3); }
            Adc_A_Dp         => { adc!(read_a,    read_dp,      write_a,    3); }
            Adc_A_DpIdxX     => { adc!(read_a,    read_dpidxx,  write_a,    4); }
            Adc_A_Abs        => { adc!(read_a,    read_abs,     write_a,    4); }
            Adc_A_AbsIdxX    => { adc!(read_a,    read_absidxx, write_a,    5); }
            Adc_A_AbsIdxY    => { adc!(read_a,    read_absidxy, write_a,    5); }
            Adc_A_Ind_DpIdxX => { adc!(read_a,    read_idxindx, write_a,    6); }
            Adc_A_IndDp_IdxY => { adc!(read_a,    read_indidxy, write_a,    6); }
            Adc_IndX_IndY    => { adc!(read_indx, read_indy,    write_indx, 5); }
            Adc_Dp_Dp        => { adc!(peek_dp,   read_dp,      write_dp,   6); } // 'peek_dp' is used to avoid fetching the location twice,
            Adc_Dp_Imm       => { adc!(peek_dp,   read_imm,     write_dp,   5); } // once for reading and once for writing

            Sbc_A_Imm        => { sbc!(read_a,    read_imm,     write_a,    2); }
            Sbc_A_IndX       => { sbc!(read_a,    read_indx,    write_a,    3); }
            Sbc_A_Dp         => { sbc!(read_a,    read_dp,      write_a,    3); }
            Sbc_A_DpIdxX     => { sbc!(read_a,    read_dpidxx,  write_a,    4); }
            Sbc_A_Abs        => { sbc!(read_a,    read_abs,     write_a,    4); }
            Sbc_A_AbsIdxX    => { sbc!(read_a,    read_absidxx, write_a,    5); }
            Sbc_A_AbsIdxY    => { sbc!(read_a,    read_absidxy, write_a,    5); }
            Sbc_A_Ind_DpIdxX => { sbc!(read_a,    read_idxindx, write_a,    6); }
            Sbc_A_IndDp_IdxY => { sbc!(read_a,    read_indidxy, write_a,    6); }
            Sbc_IndX_IndY    => { sbc!(read_indx, read_indy,    write_indx, 5); }
            Sbc_Dp_Dp        => { sbc!(peek_dp,   read_dp,      write_dp,   6); } // 'peek_dp' is used to avoid fetching the location twice,
            Sbc_Dp_Imm       => { sbc!(peek_dp,   read_imm,     write_dp,   5); } // once for reading and once for writing

            Cmp_A_Imm        => { cmp!(read_a,    read_imm,     2); }
            Cmp_A_IndX       => { cmp!(read_a,    read_indx,    3); }
            Cmp_A_Dp         => { cmp!(read_a,    read_dp,      3); }
            Cmp_A_DpIdxX     => { cmp!(read_a,    read_dpidxx,  4); }
            Cmp_A_Abs        => { cmp!(read_a,    read_abs,     4); }
            Cmp_A_AbsIdxX    => { cmp!(read_a,    read_absidxx, 5); }
            Cmp_A_AbsIdxY    => { cmp!(read_a,    read_absidxy, 5); }
            Cmp_A_Ind_DpIdxX => { cmp!(read_a,    read_idxindx, 6); }
            Cmp_A_IndDp_IdxY => { cmp!(read_a,    read_indidxy, 6); }
            Cmp_IndX_IndY    => { cmp!(read_indx, read_indy,    5); }
            Cmp_Dp_Dp        => { cmp!(read_dp,   read_dp,      6); }
            Cmp_Dp_Imm       => { cmp!(read_dp,   read_imm,     5); }

            Cmp_X_Imm => { cmp!(read_x, read_imm, 2); }
            Cmp_X_Dp  => { cmp!(read_x, read_dp,  3); }
            Cmp_X_Abs => { cmp!(read_x, read_abs, 4); }
            Cmp_Y_Imm => { cmp!(read_y, read_imm, 2); }
            Cmp_Y_Dp  => { cmp!(read_y, read_dp,  3); }
            Cmp_Y_Abs => { cmp!(read_y, read_abs, 4); }

            // Bitwise logical

            And_A_Imm        => { and!(read_a,    read_imm,     write_a,    2); }
            And_A_IndX       => { and!(read_a,    read_indx,    write_a,    3); }
            And_A_Dp         => { and!(read_a,    read_dp,      write_a,    3); }
            And_A_DpIdxX     => { and!(read_a,    read_dpidxx,  write_a,    4); }
            And_A_Abs        => { and!(read_a,    read_abs,     write_a,    4); }
            And_A_AbsIdxX    => { and!(read_a,    read_absidxx, write_a,    5); }
            And_A_AbsIdxY    => { and!(read_a,    read_absidxy, write_a,    5); }
            And_A_Ind_DpIdxX => { and!(read_a,    read_idxindx, write_a,    6); }
            And_A_IndDp_IdxY => { and!(read_a,    read_indidxy, write_a,    6); }
            And_IndX_IndY    => { and!(read_indx, read_indy,    write_indx, 5); }
            And_Dp_Dp        => { and!(peek_dp,   read_dp,      write_dp,   6); } // 'peek_dp' is used to avoid fetching the location twice,
            And_Dp_Imm       => { and!(peek_dp,   read_imm,     write_dp,   5); } // once for reading and once for writing

            Or_A_Imm        => { or!(read_a,    read_imm,     write_a,    2); }
            Or_A_IndX       => { or!(read_a,    read_indx,    write_a,    3); }
            Or_A_Dp         => { or!(read_a,    read_dp,      write_a,    3); }
            Or_A_DpIdxX     => { or!(read_a,    read_dpidxx,  write_a,    4); }
            Or_A_Abs        => { or!(read_a,    read_abs,     write_a,    4); }
            Or_A_AbsIdxX    => { or!(read_a,    read_absidxx, write_a,    5); }
            Or_A_AbsIdxY    => { or!(read_a,    read_absidxy, write_a,    5); }
            Or_A_Ind_DpIdxX => { or!(read_a,    read_idxindx, write_a,    6); }
            Or_A_IndDp_IdxY => { or!(read_a,    read_indidxy, write_a,    6); }
            Or_IndX_IndY    => { or!(read_indx, read_indy,    write_indx, 5); }
            Or_Dp_Dp        => { or!(peek_dp,   read_dp,      write_dp,   6); } // 'peek_dp' is used to avoid fetching the location twice,
            Or_Dp_Imm       => { or!(peek_dp,   read_imm,     write_dp,   5); } // once for reading and once for writing

            Eor_A_Imm        => { eor!(read_a,    read_imm,     write_a,    2); }
            Eor_A_IndX       => { eor!(read_a,    read_indx,    write_a,    3); }
            Eor_A_Dp         => { eor!(read_a,    read_dp,      write_a,    3); }
            Eor_A_DpIdxX     => { eor!(read_a,    read_dpidxx,  write_a,    4); }
            Eor_A_Abs        => { eor!(read_a,    read_abs,     write_a,    4); }
            Eor_A_AbsIdxX    => { eor!(read_a,    read_absidxx, write_a,    5); }
            Eor_A_AbsIdxY    => { eor!(read_a,    read_absidxy, write_a,    5); }
            Eor_A_Ind_DpIdxX => { eor!(read_a,    read_idxindx, write_a,    6); }
            Eor_A_IndDp_IdxY => { eor!(read_a,    read_indidxy, write_a,    6); }
            Eor_IndX_IndY    => { eor!(read_indx, read_indy,    write_indx, 5); }
            Eor_Dp_Dp        => { eor!(peek_dp,   read_dp,      write_dp,   6); } // 'peek_dp' is used to avoid fetching the location twice,
            Eor_Dp_Imm       => { eor!(peek_dp,   read_imm,     write_dp,   5); } // once for reading and once for writing

            // Increment/Decrement

            Inc_A      => { inc!(read_a,      write_a,      2); }
            Inc_Dp     => { inc!(peek_dp,     write_dp,     4); }
            Inc_DpIdxX => { inc!(peek_dpidxx, write_dpidxx, 5); }
            Inc_Abs    => { inc!(peek_abs,    write_abs,    5); }
            Inc_X      => { inc!(read_x,      write_x,      2); }
            Inc_Y      => { inc!(read_y,      write_y,      2); }
            Dec_A      => { dec!(read_a,      write_a,      2); }
            Dec_Dp     => { dec!(peek_dp,     write_dp,     4); }
            Dec_DpIdxX => { dec!(peek_dpidxx, write_dpidxx, 5); }
            Dec_Abs    => { dec!(peek_abs,    write_abs,    5); }
            Dec_X      => { dec!(read_x,      write_x,      2); }
            Dec_Y      => { dec!(read_y,      write_y,      2); }

            // Shifts

            Asl_A      => { asl!(read_a,      write_a,      2); }
            Asl_Dp     => { asl!(peek_dp,     write_dp,     4); }
            Asl_DpIdxX => { asl!(peek_dpidxx, write_dpidxx, 5); }
            Asl_Abs    => { asl!(peek_abs,    write_abs,    5); }
            Lsr_A      => { lsr!(read_a,      write_a,      2); }
            Lsr_Dp     => { lsr!(peek_dp,     write_dp,     4); }
            Lsr_DpIdxX => { lsr!(peek_dpidxx, write_dpidxx, 5); }
            Lsr_Abs    => { lsr!(peek_abs,    write_abs,    5); }
            Rol_A      => { rol!(read_a,      write_a,      2); }
            Rol_Dp     => { rol!(peek_dp,     write_dp,     4); }
            Rol_DpIdxX => { rol!(peek_dpidxx, write_dpidxx, 5); }
            Rol_Abs    => { rol!(peek_abs,    write_abs,    5); }
            Ror_A      => { ror!(read_a,      write_a,      2); }
            Ror_Dp     => { ror!(peek_dp,     write_dp,     4); }
            Ror_DpIdxX => { ror!(peek_dpidxx, write_dpidxx, 5); }
            Ror_Abs    => { ror!(peek_abs,    write_abs,    5); }

            Xcn_A => {
                let value = state.a;
                let result = value.wrapping_shl(4) | value.wrapping_shr(4);
                state.a = result;
                state.apply_nz(result);
                state.busy_cycles += 5;
            }

            // 16-bit load/store

            Movw_YA_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value: u16 = Cpu::dp_read16(state, bus, loc);
                state.set_ya(value);
                state.set_z(value == 0);
                state.set_n((value | 0x8000) != 0);
                state.busy_cycles += 5;
            }
            Movw_Dp_YA => {
                let loc = Cpu::fetch(state, bus);
                let value: u16 = state.ya();
                Cpu::dp_write16(state, bus, loc, value);
                state.busy_cycles += 5;
            }

            // 16-bit math

            Incw_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value: u16 = Cpu::dp_read16(state, bus, loc);
                let result = value.wrapping_add(1);
                Cpu::dp_write16(state, bus, loc, result);
                state.set_z(result == 0);
                state.set_n((result & 0x8000) != 0);
                state.busy_cycles += 6;
            }
            Decw_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value: u16 = Cpu::dp_read16(state, bus, loc);
                let result = value.wrapping_sub(1);
                Cpu::dp_write16(state, bus, loc, result);
                state.set_z(result == 0);
                state.set_n((result & 0x8000) != 0);
                state.busy_cycles += 6;
            }
            Addw_YA_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value1: u16 = state.ya();
                let value2: u16 = Cpu::dp_read16(state, bus, loc);

                let (result, carry) = value1.overflowing_add(value2); // No carry, apparently, in 16-bit add
                let half_carry = (((value1 & 0x0fff) + (value2 & 0x0fff)) & 0x1000) != 0;
                let overflow = (value1.sign() == value2.sign()) && (value1.sign() != result.sign());
                state.set_z(result == 0);
                state.set_n(result.sign());
                state.set_c(carry);
                state.set_h(half_carry);
                state.set_v(overflow);

                state.set_ya(result);
                state.busy_cycles += 5;
            }
            Subw_YA_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value1: u16 = state.ya();
                let value2: u16 = Cpu::dp_read16(state, bus, loc);

                let (result, borrow) = value1.overflowing_sub(value2); // No borrow, apparently, in 16-bit sub
                let half_borrow = (value1 & 0x0fff) < ((value2 & 0x0fff));
                let overflow = (value1.sign() != value2.sign()) && (value1.sign() != result.sign());
                state.set_z(result == 0);
                state.set_n(result.sign());
                state.set_c(!borrow);
                state.set_h(!half_borrow);
                state.set_v(overflow);

                state.set_ya(result);
                state.busy_cycles += 5;
            }
            Cmpw_YA_Dp => {
                let loc = Cpu::fetch(state, bus);
                let value1: u16 = state.ya();
                let value2: u16 = Cpu::dp_read16(state, bus, loc);

                let (result, borrow) = value1.overflowing_sub(value2);
                state.set_z(result == 0);
                state.set_n(result.sign());
                state.set_c(!borrow);

                state.busy_cycles += 4;
            }
            Mul_YA => {
                let value1 = state.y as u16;
                let value2 = state.a as u16;
                let result = value1 * value2;
                state.set_ya(result);
                state.set_z(result == 0);
                state.set_n(result.sign());
                state.busy_cycles += 9;
            }
            Div_YA_X => {
                // TODO this operation has some odd caveats where the results are invalid if the inputs are in a certain range.
                // I have not implemented that here!
                let dividend = state.ya();
                let divisor = state.x as u16;
                // TODO is the handling of divide by zero correct?
                let result: u8 = if divisor == 0 { 0xff } else { (dividend / divisor) as u8 }; // TODO wrapping of the result (if > 0xff) may not be the right thing to do!
                let remainder: u8 = if divisor == 0 { 0x00 } else { (dividend % divisor) as u8 };
                state.a = result;
                state.y = remainder;
                state.set_z(result == 0);
                state.set_n(result.sign());
                // TODO V and H are also affected but I don't know how
                state.busy_cycles += 12;
            }

            // Decimal adjust

            Daa_A => {
                panic!("DAA is unimplemented")
                //state.busy_cycles += 3;
            }
            Das_A => {
                panic!("DAS is unimplemented")
                //state.busy_cycles += 3;
            }

            // Program flow

            Bra     => { branch_if!(true); }
            Beq     => { branch_if!( state.z()); }
            Bne     => { branch_if!(!state.z()); }
            Bcs     => { branch_if!( state.c()); }
            Bcc     => { branch_if!(!state.c()); }
            Bvs     => { branch_if!( state.v()); }
            Bvc     => { branch_if!(!state.v()); }
            Bmi     => { branch_if!( state.n()); }
            Bpl     => { branch_if!(!state.n()); }
            Bbs0_Dp => { Cpu::bbs(state, bus, 0); }
            Bbs1_Dp => { Cpu::bbs(state, bus, 1); }
            Bbs2_Dp => { Cpu::bbs(state, bus, 2); }
            Bbs3_Dp => { Cpu::bbs(state, bus, 3); }
            Bbs4_Dp => { Cpu::bbs(state, bus, 4); }
            Bbs5_Dp => { Cpu::bbs(state, bus, 5); }
            Bbs6_Dp => { Cpu::bbs(state, bus, 6); }
            Bbs7_Dp => { Cpu::bbs(state, bus, 7); }
            Bbc0_Dp => { Cpu::bbc(state, bus, 0); }
            Bbc1_Dp => { Cpu::bbc(state, bus, 1); }
            Bbc2_Dp => { Cpu::bbc(state, bus, 2); }
            Bbc3_Dp => { Cpu::bbc(state, bus, 3); }
            Bbc4_Dp => { Cpu::bbc(state, bus, 4); }
            Bbc5_Dp => { Cpu::bbc(state, bus, 5); }
            Bbc6_Dp => { Cpu::bbc(state, bus, 6); }
            Bbc7_Dp => { Cpu::bbc(state, bus, 7); }
            Cbne_Dp => {
                let loc = Cpu::fetch(state, bus);
                let amount = Cpu::fetch(state, bus) as i8;
                let value = Cpu::dp_read(state, bus, loc);
                if state.a != value { state.pc = state.pc.signed_add(amount); state.busy_cycles += 7; } else { state.busy_cycles += 5; }
            }
            Cbne_DpIdxX => {
                let loc = Cpu::fetch(state, bus).wrapping_add(state.x);
                let amount = Cpu::fetch(state, bus) as i8;
                let value = Cpu::dp_read(state, bus, loc);
                if state.a != value { state.pc = state.pc.signed_add(amount); state.busy_cycles += 8; } else { state.busy_cycles += 6; }
            }
            Dbnz_Dp => {
                let loc = Cpu::fetch(state, bus);
                let amount = Cpu::fetch(state, bus) as i8;
                let value = Cpu::dp_read(state, bus, loc);
                let result = value.wrapping_sub(1);
                Cpu::dp_write(state, bus, loc, result);
                if result != 0 { state.pc = state.pc.signed_add(amount); state.busy_cycles += 7; } else { state.busy_cycles += 5; }
            }
            Dbnz_Y => {
                let amount = Cpu::fetch(state, bus) as i8;
                let value = state.y;
                let result = value.wrapping_sub(1);
                state.y = result;
                if result != 0 { state.pc = state.pc.signed_add(amount); state.busy_cycles += 6; } else { state.busy_cycles += 4; }
            }
            Jmp => {
                let addr = Cpu::fetch16(state, bus);
                state.pc = addr;
                state.busy_cycles += 3;
            }
            Jmp_Ind_AbsIdxX => {
                let ind = Cpu::fetch16(state, bus).wrapping_add(state.x as u16);
                let addr = Cpu::read16(state, bus, ind);
                state.pc = addr;
                state.busy_cycles += 6;
            }

            // Subroutine calls

            Call => {
                let addr = Cpu::fetch16(state, bus);
                Cpu::call(state, bus, addr);
                state.busy_cycles += 8;
            }
            Pcall => {
                let loc = Cpu::fetch(state, bus);
                let addr = u16::hi_lo(0xff, loc);
                Cpu::call(state, bus, addr);
                state.busy_cycles += 6;
            }
            Tcall0 => { Cpu::call(state, bus, 0xffde); state.busy_cycles += 8; }
            Tcall1 => { Cpu::call(state, bus, 0xffdc); state.busy_cycles += 8; }
            Tcall2 => { Cpu::call(state, bus, 0xffda); state.busy_cycles += 8; }
            Tcall3 => { Cpu::call(state, bus, 0xffd8); state.busy_cycles += 8; }
            Tcall4 => { Cpu::call(state, bus, 0xffd6); state.busy_cycles += 8; }
            Tcall5 => { Cpu::call(state, bus, 0xffd4); state.busy_cycles += 8; }
            Tcall6 => { Cpu::call(state, bus, 0xffd2); state.busy_cycles += 8; }
            Tcall7 => { Cpu::call(state, bus, 0xffd0); state.busy_cycles += 8; }
            Tcall8 => { Cpu::call(state, bus, 0xffce); state.busy_cycles += 8; }
            Tcall9 => { Cpu::call(state, bus, 0xffcc); state.busy_cycles += 8; }
            TcallA => { Cpu::call(state, bus, 0xffca); state.busy_cycles += 8; }
            TcallB => { Cpu::call(state, bus, 0xffc8); state.busy_cycles += 8; }
            TcallC => { Cpu::call(state, bus, 0xffc6); state.busy_cycles += 8; }
            TcallD => { Cpu::call(state, bus, 0xffc4); state.busy_cycles += 8; }
            TcallE => { Cpu::call(state, bus, 0xffc2); state.busy_cycles += 8; }
            TcallF => { Cpu::call(state, bus, 0xffc0); state.busy_cycles += 8; }
            Brk => {
                Cpu::call(state, bus, 0xffde);
                let psw = state.psw;
                Cpu::push(state, bus, psw);
                state.set_b(true);
                state.set_i(false);
                state.busy_cycles += 8;
            }
            Ret => {
                let ret_addr = Cpu::popw(state, bus);
                state.pc = ret_addr;
                state.busy_cycles += 5;
            }
            Ret1 => {
                let psw = Cpu::pop(state, bus);
                let ret_addr = Cpu::popw(state, bus);
                state.psw = psw;
                state.pc = ret_addr;
                state.busy_cycles += 6;
            }
            Push_A => {
                let value = state.a;
                Cpu::push(state, bus, value);
                state.busy_cycles += 4;
            }
            Push_X => {
                let value = state.x;
                Cpu::push(state, bus, value);
                state.busy_cycles += 4;
            }
            Push_Y => {
                let value = state.y;
                Cpu::push(state, bus, value);
                state.busy_cycles += 4;
            }
            Push_Psw => {
                let value = state.psw;
                Cpu::push(state, bus, value);
                state.busy_cycles += 4;
            }
            Pop_A => {
                let value = Cpu::pop(state, bus);
                state.a = value;
                state.busy_cycles += 4;
            }
            Pop_X => {
                let value = Cpu::pop(state, bus);
                state.x = value;
                state.busy_cycles += 4;
            }
            Pop_Y => {
                let value = Cpu::pop(state, bus);
                state.y = value;
                state.busy_cycles += 4;
            }
            Pop_Psw => {
                let value = Cpu::pop(state, bus);
                state.psw = value;
                state.busy_cycles += 4;
            }

            // Bit operations

            Set0_Dp => { Cpu::setn(state, bus, 0); }
            Set1_Dp => { Cpu::setn(state, bus, 1); }
            Set2_Dp => { Cpu::setn(state, bus, 2); }
            Set3_Dp => { Cpu::setn(state, bus, 3); }
            Set4_Dp => { Cpu::setn(state, bus, 4); }
            Set5_Dp => { Cpu::setn(state, bus, 5); }
            Set6_Dp => { Cpu::setn(state, bus, 6); }
            Set7_Dp => { Cpu::setn(state, bus, 7); }
            Clr0_Dp => { Cpu::clrn(state, bus, 0); }
            Clr1_Dp => { Cpu::clrn(state, bus, 1); }
            Clr2_Dp => { Cpu::clrn(state, bus, 2); }
            Clr3_Dp => { Cpu::clrn(state, bus, 3); }
            Clr4_Dp => { Cpu::clrn(state, bus, 4); }
            Clr5_Dp => { Cpu::clrn(state, bus, 5); }
            Clr6_Dp => { Cpu::clrn(state, bus, 6); }
            Clr7_Dp => { Cpu::clrn(state, bus, 7); }
            Tset1 => {
                let addr = Cpu::fetch16(state, bus);
                let value = bus.read(addr);
                let result = value | state.a;
                let cmp_res = state.a.wrapping_sub(value);
                bus.write(addr, result);
                state.apply_nz(cmp_res);
                state.busy_cycles += 6;
            }
            Tclr1 => {
                let addr = Cpu::fetch16(state, bus);
                let value = bus.read(addr);
                let result = value & !state.a;
                let cmp_res = state.a.wrapping_sub(value);
                bus.write(addr, result);
                state.apply_nz(cmp_res);
                state.busy_cycles += 6;
            }
            And1_C_Mem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let old_carry = state.c();
                state.set_c(old_carry & value.test(bit_nr));
                state.busy_cycles += 4;
            }
            And1_C_NotMem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let old_carry = state.c();
                state.set_c(old_carry & !value.test(bit_nr));
                state.busy_cycles += 4;
            }
            Or1_C_Mem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let old_carry = state.c();
                state.set_c(old_carry | value.test(bit_nr));
                state.busy_cycles += 5;
            }
            Or1_C_NotMem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let old_carry = state.c();
                state.set_c(old_carry | !value.test(bit_nr));
                state.busy_cycles += 5;
            }
            Eor1_C_Mem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let old_carry = state.c();
                state.set_c(old_carry ^ value.test(bit_nr));
                state.busy_cycles += 5;
            }
            Not1_Mem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let result = value ^ (0x01 << bit_nr); // flip (negate) the bit number requested
                bus.write(addr, result);
                state.busy_cycles += 5;
            }
            Mov1_C_Mem => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                state.set_c(value.test(bit_nr));
                state.busy_cycles += 4;
            }
            Mov1_Mem_C => {
                let argument = Cpu::fetch16(state, bus);
                let addr = argument & 0x1fff;
                let bit_nr = ((argument >> 13) & 0x0007) as u8;
                let value = bus.read(addr);
                let result = value.modify_bit(bit_nr, state.c());
                bus.write(addr, result);
                state.busy_cycles += 6;
            }

            // Flag manipulation

            Clrc => { state.set_c(false); state.busy_cycles += 2; }
            Setc => { state.set_c(true); state.busy_cycles += 2; }
            Notc => { let carry = state.c(); state.set_c(!carry); state.busy_cycles += 3; }
            Clrv => { state.set_v(false); state.set_h(false); state.busy_cycles += 2; }
            Clrp => { state.set_p(false); state.busy_cycles += 2; }
            Setp => { state.set_p(true); state.busy_cycles += 2; }
            Ei   => { state.set_i(true); state.busy_cycles += 3; }
            Di   => { state.set_i(false); state.busy_cycles += 3; }

            // Misc
            Nop   => { state.busy_cycles += 2; }
            Sleep => { state.halted = true; state.busy_cycles += 3; }
            Stop  => { state.halted = true; state.busy_cycles += 3; }
        }
    }

    pub fn instr_as_string(state: &mut CpuState, bus: &A) -> String {
        use op::INSTR_LENGTHS;
        let pc = state.pc;
        let opcode = bus.read(pc);
        let arg1 = bus.read(pc.wrapping_add(1));
        let arg2 = bus.read(pc.wrapping_add(2));
        let nr_bytes = INSTR_LENGTHS[opcode as usize];
        let instr = Instruction::from_opcode(opcode);
        match nr_bytes {
            1 => format!("{:?}", instr),
            2 => format!("{:?} {:02x}", instr, arg1),
            3 => format!("{:?} {:04x}", instr, u16::hi_lo(arg2, arg1)),
            -2 => { // signed offset
                let target = pc.wrapping_add(2).signed_add(arg1 as i8);
                format!("{:?} {:04x}", instr, target)
            },
            _ => "???".to_string()
        }
    }
}



impl<'a, A: Addressable + ?Sized> Clockable for Cpu<'a, A> {
    fn tick(&mut self) {
        let state: &mut CpuState = &mut (*self.state.borrow_mut());
        if state.busy_cycles == 0 {
            let bus = self.bus_ref;
            if state.halted { panic!("The CPU is halted!"); }
//            { // DEBUG
//                let instr_string = Cpu::instr_as_string(state, bus);
//                println!("{} {:04x} {}", state, state.pc, instr_string);
//            }
            let opcode = Cpu::fetch(state, bus);
            let instr = Instruction::from_opcode(opcode);
            Cpu::execute(state, bus, instr);
        }
        state.busy_cycles = state.busy_cycles.saturating_sub(1);
    }
}
