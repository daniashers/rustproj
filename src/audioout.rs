extern crate cpal;
use std;
use self::cpal::Endpoint;
use self::cpal::Format;
use self::cpal::ChannelPosition;
use self::cpal::SamplesRate;
use util::StereoSample;

pub fn enumerate() {
    let endpoints = cpal::endpoints();

    println!("Endpoints: ");
    for (endpoint_index, endpoint) in endpoints.enumerate() {
        println!("{}. Endpoint \"{}\" Audio formats: ",
                 endpoint_index + 1,
                 endpoint.name());

        let formats = match endpoint.supported_formats() {
            Ok(f) => f,
            Err(e) => {
                println!("Error: {:?}", e);
                continue;
            },
        };

        for (format_index, format) in formats.enumerate() {
            println!("{}.{}. {:?}", endpoint_index + 1, format_index + 1, format);
        }
    }
}

pub fn start_output_loop<F>(mut gen_samples: F) where F: FnMut(usize) -> Vec<StereoSample> { // F: nr_samples_required -> vec with samples

    let endpoint: Endpoint = cpal::default_endpoint().expect("Failed to get default endpoint");

//    let wanted_format = Format {
//        channels: vec!(ChannelPosition::FrontLeft, ChannelPosition::FrontRight),
//        samples_rate: SamplesRate(32000),
//        data_type: SampleFormat::I16
//    };


    let format: Format = endpoint
        .supported_formats()
        .unwrap()
        .next()
        .expect("Failed to get endpoint format")
        .with_max_samples_rate();

    let event_loop = cpal::EventLoop::new();
    let voice_id = event_loop.build_voice(&endpoint, &format).unwrap();
    event_loop.play(voice_id);

    let samples_rate = format.samples_rate.0 as f32;

    event_loop.run(move |_, buffer| {
        match buffer {
            cpal::UnknownTypeBuffer::U16(mut buffer) => {
                unimplemented!();
//                for sample in buffer.chunks_mut(format.channels.len()) {
//                    let value = ((next_value() * 0.5 + 0.5) * std::u16::MAX as f32) as u16;
//                    for out in sample.iter_mut() {
//                        *out = value;
//                    }
//                }
            },

            cpal::UnknownTypeBuffer::I16(mut buffer) => {
                let iter = buffer.chunks_mut(2 /* stereo */);
                let nr_to_fill = iter.len();
                let samples:  Vec<StereoSample> = gen_samples(nr_to_fill);
                let zipped = samples.iter().zip(iter);
                for (sample, out) in zipped {
                    out[0] = sample.0;
                    out[1] = sample.1;
                }
            },

            cpal::UnknownTypeBuffer::F32(mut buffer) => {
                let iter = buffer.chunks_mut(2 /* stereo */);
                let nr_to_fill = iter.len();
                let samples:  Vec<StereoSample> = gen_samples(nr_to_fill);
                let zipped = samples.iter().zip(iter);
                for (sample, out) in zipped {
                    out[0] = (sample.0 as f32) / 32768.0;
                    out[1] = (sample.1 as f32) / 32768.0;
                }
            },
        };
    });
}