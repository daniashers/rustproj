pub type StereoSample = (i16, i16);

pub trait RichByte {
    fn msb(self) -> bool;
    fn lsb(self) -> bool;
    fn modify_msb(self, value: bool) -> Self;
    fn modify_lsb(self, value: bool) -> Self;
    fn sign(self) -> bool;
    fn test(self, bit: u8) -> bool;
    fn modify_bit(self, bit: u8, value: bool) -> u8;
    fn for_each_bit<A, F>(self, mut f: F) -> [A; 8] where F: FnMut(u8, bool) -> A;
    fn make_bitmap<F>(f: F) -> Self where F: Fn(usize) -> bool;
}

impl RichByte for u8 {
    fn msb(self) -> bool {
        (self & 0b10000000) != 0
    }
    fn lsb(self) -> bool {
        (self & 0b00000001) != 0
    }
    fn modify_msb(self, value: bool) -> u8 {
        self.modify_bit(7, value)
    }
    fn modify_lsb(self, value: bool) -> u8 {
        self.modify_bit(0, value)
    }
    fn sign(self) -> bool {
        (self & 0x80) != 0
    }
    fn test(self, bit: u8) -> bool {
        let mask = 1 << bit;
        (self & mask) != 0
    }
    fn modify_bit(self, bit: u8, value: bool) -> u8 {
        let mask = 1 << bit;
        if value { self | mask } else { self & !mask }
    }
    fn for_each_bit<A, F>(self, mut f: F) -> [A; 8] where F: FnMut(u8, bool) -> A {
        [
            f(0, self.test(0)), f(1, self.test(1)), f(2, self.test(2)), f(3, self.test(3)),
            f(4, self.test(4)), f(5, self.test(5)), f(6, self.test(6)), f(7, self.test(7))
        ]
    }
    fn make_bitmap<F>(f: F) -> u8 where F: Fn(usize) -> bool {
        0x00
            .modify_bit(0, f(0))
            .modify_bit(1, f(1))
            .modify_bit(2, f(2))
            .modify_bit(3, f(3))
            .modify_bit(4, f(4))
            .modify_bit(5, f(5))
            .modify_bit(6, f(6))
            .modify_bit(7, f(7))
    }

}

pub trait RichWord {
    fn hi_lo(hi: u8, lo: u8) -> u16;
    fn hi(self) -> u8;
    fn lo(self) -> u8;
    fn sign(self) -> bool;
    fn signed_add(self, other: i8) -> u16;
    fn clamp_ceil(self, limit: u16) -> u16;
}

impl RichWord for u16 {
    fn hi_lo(hi: u8, lo: u8) -> u16 {
        ((hi as u16) << 8) | (lo as u16)
    }

    fn hi(self) -> u8 {
        (self >> 8) as u8
    }
    fn lo(self) -> u8 {
        (self & 0x00ff) as u8
    }
    fn sign(self) -> bool {
        (self & 0x8000) != 0
    }
    fn signed_add(self, other: i8) -> u16 {
        if other > 0 { self + (other as u16) }
            else if other == i8::min_value() { self - 128 }
                else { self - (-other) as u16 }
    }
    fn clamp_ceil(self, limit: u16) -> u16 {
        if self <= limit { self } else { limit }
    }
}

pub trait RichBool {
    fn as_digit(self) -> u8;
}

impl RichBool for bool {
    fn as_digit(self) -> u8 {
        if self { 1 } else { 0 }
    }
}

pub trait RichVec {
    fn drop(&self, nr_elements: usize) -> Self;
}

impl<A> RichVec for Vec<A> where A: Clone {
    fn drop(&self, nr_elements: usize) -> Vec<A> {
        if nr_elements >= self.len() { vec!() }
        else { self[nr_elements..].to_vec() }
    }
}

pub trait RichStereoSample {
    fn saturating_add(self, other: Self) -> Self;
    fn adjust_volume(self, factor_l: i8, factor_r: i8) -> Self; // signed volume means it can invert the phase
}

// This volume adjustment is done according to the DSP's algorithm.
impl RichStereoSample for StereoSample {
    fn saturating_add(self, other: StereoSample) -> StereoSample {
        (self.0.saturating_add(other.0), self.1.saturating_add(other.1))
    }

    /// The factors for volume adjustments are signed 8-bit values where +7f (max value) means 1.0 (full volume)
    /// and lesser values lower volume proportionally. Negative values are possible (sign of sample is inverted)
    fn adjust_volume(self, factor_l: i8, factor_r: i8) -> StereoSample {
        let l: i16 = (((self.0 as i32) * (factor_l as i32)) >> 7) as i16; // TODO The conversion back to i16 SHOULD be in a valid range except for
        let r: i16 = (((self.1 as i32) * (factor_r as i32)) >> 7) as i16; // TODO sample = -32768 and vol = -128. Unusual enough? Shall I risk it?
        (l, r)
    }
}

pub trait RichDuration {
    fn format_nanos(self) -> String;
}

impl RichDuration for u64 {
    fn format_nanos(self) -> String {
        let floating: f32 = self as f32;
        if self < 1_000 { format!("{:3}ns", self) }
        else if self < 10_000 { format!("{:3.1}us", floating/1_000.0) }
        else if self < 1_000_000 { format!("{:3}us", self/1_000) }
        else if self < 10_000_000 { format!("{:3.1}ms", floating/1_000_000.0) }
        else if self < 1_000_000_000 { format!("{:3}ms", self/1_000_000) }
        else if self < 10_000_000_000 { format!("{:3.1} s", floating/1_000_000_000.0) }
        else { format!("{:3} s", self/1_000_000_000) }

    }
}