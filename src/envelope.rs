use dsp::COUNTER_RATES;
use util::*;

#[derive(PartialEq)]
pub enum EnvelopePhase {
    Attack,
    Decay,
    Sustain,
    Release // TODO technically, release is not really part of the envelope in that it applies EVEN IF THE MODE IS NOT ADSR. Maybe remove from here?
}

#[derive(PartialEq)]
#[derive(Debug)]
pub enum EnvelopeMode {
    Direct,
    Gain,
    Adsr
}

pub enum GainMode {
    LinearDecrease,
    ExpDecrease,
    LinearIncrease,
    BentIncrease
}


pub struct Envelope {
    pub adsr1: u8, // raw register value
    pub adsr2: u8, // raw register value
    pub gain: u8,  // raw register value

    pub adsr_phase: EnvelopePhase,

    pub current_value: u16, // this is actually only 11 bits.

    internal_counter: u16 // A free-running counter wrapping at 0x7800 used to enforce the update period.
                          // 0x7800 is chosen because it is divisible by each of the possible periods, thus ensuring uniform timing.
}

impl Envelope {
    pub fn new() -> Envelope {
        Envelope {
            adsr1: 0,
            adsr2: 0,
            gain: 0,
            adsr_phase: EnvelopePhase::Release,
            current_value: 0,
            internal_counter: 0
        }
    }

    pub fn restart(&mut self) {
        self.current_value = 0;
        self.adsr_phase = EnvelopePhase::Attack;
    }

    pub fn envelope_mode(&self) -> EnvelopeMode {
        if self.adsr1.test(7) { EnvelopeMode::Adsr }
        else {
            if self.gain.test(7) { EnvelopeMode::Gain } else { EnvelopeMode::Direct }
        }
    }

    // This is the function to call at every sample.
    pub fn tick(&mut self) {
        if self.adsr_phase == EnvelopePhase::Release {
            // if the envelope is in Release state, this overrides any other setting!
            self.current_value = self.current_value.saturating_sub(8);
        } else if self.envelope_mode() == EnvelopeMode::Direct {
            // If Direct mode is selected, we will update the level at every tick, without waiting for the next event!
            self.current_value = self.direct_mode_level();
        }

        if self.internal_counter == MAX_INTERNAL_COUNTER {
            self.internal_counter = 0;
        } else {
            self.internal_counter += 1;
        }
        let update_period = self.update_period();
        if (self.adsr_phase != EnvelopePhase::Release) && (update_period != 0) && ((self.internal_counter % update_period) == 0) { // update_period == 0 means never update (infinite period)
            self.update_envelope();
        }
    }

    // This function is NOT the one to call at every sample. It only gets called when an update is actually due.
    fn update_envelope(&mut self) {
        match self.envelope_mode() {
            EnvelopeMode::Direct => {
                self.current_value = self.direct_mode_level();
            }
            EnvelopeMode::Gain => {
                match self.gain_mode() {
                    GainMode::LinearDecrease => { self.current_value = self.current_value.saturating_sub(32); },
                    GainMode::ExpDecrease => { self.decay_exponentially(); }
                    GainMode::LinearIncrease => { self.current_value = self.current_value.saturating_add(32).clamp_ceil(MAX_ENVELOPE); }
                    GainMode::BentIncrease => {
                        if self.current_value < 0x600 {
                            self.current_value = self.current_value.saturating_add(32).clamp_ceil(MAX_ENVELOPE);
                        } else {
                            self.current_value = self.current_value.saturating_add(8).clamp_ceil(MAX_ENVELOPE);
                        }
                    }
                }
            }
            EnvelopeMode::Adsr => {
                match self.adsr_phase {
                    EnvelopePhase::Attack => {
                        self.current_value += self.attack_increment();
                        if self.current_value > MAX_ENVELOPE {
                            self.current_value = MAX_ENVELOPE;
                            self.adsr_phase = EnvelopePhase::Decay;
                        }
                    }
                    EnvelopePhase::Decay => {
                        self.decay_exponentially();
                        if (self.current_value >> 8) <= (self.sustain_level() as u16) { // if it matches or has gone below the sustain level... (only the top 3 significant bits are checked)
                            self.adsr_phase = EnvelopePhase::Sustain;
                        }
                    }
                    EnvelopePhase::Sustain => {
                        self.decay_exponentially();
                    }
                    EnvelopePhase::Release => {
                        // If the envelope were in the release stage, we would have already decremented it
                        // earlier (in the 'tick' method) since being in the Release state override everything else.
                        // Thus if this were the case, we would have done the decrement already.
//                        self.current_envelope_value = self.current_envelope_value.saturating_sub(8);
                    }
                }
            }
        }
    }

    // The amount of samples to wait between each envelope update. Zero means never update.
    fn update_period(&self) -> u16 {
        match self.envelope_mode() {
            EnvelopeMode::Direct => 0, // never
            EnvelopeMode::Gain => self.gain_rate(),
            EnvelopeMode::Adsr => {
                match self.adsr_phase {
                    EnvelopePhase::Attack  => self.attack_rate(),
                    EnvelopePhase::Decay   => self.decay_rate(),
                    EnvelopePhase::Sustain => self.sustain_rate(),
                    EnvelopePhase::Release => 1 // every sample
                }
            }
        }
    }

    /// Returns the level the envelope should have when in 'Direct' mode.
    fn direct_mode_level(&self) -> u16 {
        ((self.gain & 0x7f) as u16) << 4
    }

    fn attack_bits(&self) -> u8 { self.adsr1 & 0x0f }
    fn decay_bits(&self) -> u8 { (self.adsr1 >> 4) & 0x07 }
    fn sustain_level_bits(&self) -> u8 { (self.adsr2 >> 5) & 0x07 }
    fn sustain_rate_bits(&self) -> u8 { (self.adsr2) & 0x1f }


    // The 'rate' (every how many samples) at which the envelope will be incremented during the Attack phase.
    fn attack_rate(&self) -> u16 {
        let attack_bits = self.attack_bits();
        if attack_bits == 0x0f { 1 } else { COUNTER_RATES[((attack_bits << 1) + 1) as usize] }
    }

    fn attack_increment(&self) -> u16 {
        if self.attack_bits() == 0x0f { 1024 } else { 32 }
    }

    // The 'rate' (every how many samples) at which the envelope will be decremented during the Decay phase.
    fn decay_rate(&self) -> u16 {
        COUNTER_RATES[(16 + ((self.adsr1 >> 4) & 0x07) * 2) as usize]
    }

    // Only 3 significant bits of this level are returned.
    fn sustain_level(&self) -> u8 {
        (self.adsr2 >> 5) & 0x07
        // TODO: from the docs: "the "lll" bits are the Sustain Level only when bit 'e' is set.
        // If 'e' is clear, the top 3 bits of VxGAIN are used instead."
        // This doesn't make any sense to me.
    }

    // The 'rate' (every how many samples) at which the envelope will be decremented during the Sustain phase.
    fn sustain_rate(&self) -> u16 {
        COUNTER_RATES[self.sustain_rate_bits() as usize]
    }

    // The 'rate' (every how many samples) at which the envelope will be updated when in 'Gain' mode.
    fn gain_rate(&self) -> u16 {
        COUNTER_RATES[((self.gain) & 0x1f) as usize]
    }

    fn gain_mode(&self) -> GainMode {
        match (self.gain >> 5) & 0x03 {
            0 => GainMode::LinearDecrease,
            1 => GainMode::ExpDecrease,
            2 => GainMode::LinearIncrease,
            3 => GainMode::BentIncrease,
            _ => panic!("Not possible")
        }
    }

    fn decay_exponentially(&mut self) {
        let decrement =
            if self.current_value == 0 {
                0
            } else {
                ((self.current_value - 1) >> 8) + 1
            };
        self.current_value -= decrement;
    }

    pub fn debug_info(&self) -> String {
        use std::ops::Add;

        let part1 = match self.envelope_mode() {
            EnvelopeMode::Direct => {
                format!("drct {:04x} ", self.direct_mode_level())
            }
            EnvelopeMode::Gain => {
                match self.gain_mode() {
                    GainMode::LinearDecrease => format!("lin-   {:02x} ", self.gain & 0x1f),
                    GainMode::LinearIncrease => format!("lin+   {:02x} ", self.gain & 0x1f),
                    GainMode::ExpDecrease    => format!("exp-   {:02x} ", self.gain & 0x1f),
                    GainMode::BentIncrease   => format!("pcw+   {:02x} ", self.gain & 0x1f)
                }
            }
            EnvelopeMode::Adsr => {
                match self.adsr_phase {
                    EnvelopePhase::Attack  => format!("Adsr {:01x}{:01x}{:01x}{:02x}", self.attack_bits(), self.decay_bits(), self.sustain_level_bits(), self.sustain_rate_bits()),
                    EnvelopePhase::Decay   => format!("aDsr {:01x}{:01x}{:01x}{:02x}", self.attack_bits(), self.decay_bits(), self.sustain_level_bits(), self.sustain_rate_bits()),
                    EnvelopePhase::Sustain => format!("adSr {:01x}{:01x}{:01x}{:02x}", self.attack_bits(), self.decay_bits(), self.sustain_level_bits(), self.sustain_rate_bits()),
                    EnvelopePhase::Release => format!("adsR {:01x}{:01x}{:01x}{:02x}", self.attack_bits(), self.decay_bits(), self.sustain_level_bits(), self.sustain_rate_bits())
                }
            }
        };
        if self.adsr_phase == EnvelopePhase::Release {
            part1.add(&format!(" {:02x}R", self.current_value >> 4))
            // when state==release we don't care about the least significatn nybble so much, we'd rather show a flag
            // that the Release state is active as that is important and overrides all else.
        } else {
            part1.add(&format!(" {:03x}", self.current_value))
        }
    }
}

static MAX_ENVELOPE: u16 = 0x7ff;
static MAX_INTERNAL_COUNTER: u16 = 0x77ff;

