use clock::Clockable;

pub struct Timer {
    pub active: bool,
    pub programmable_value: u8,
    low_counter: u8,
    high_counter: u8
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            active: false,
            programmable_value: 0,
            low_counter: 0,
            high_counter: 0
        }
    }

    // This has the side effect of resetting the counter.
    pub fn read_counter(&mut self) -> u8 {
        let value = self.high_counter & 0x0f;
        self.high_counter = 0;
        value
    }
}

impl Clockable for Timer {
    fn tick(&mut self) {
        if self.active {
            self.low_counter = self.low_counter.wrapping_add(1);
            if self.active && (self.low_counter == self.programmable_value) {
                self.low_counter = 0;
                self.high_counter = self.high_counter.wrapping_add(1) & 0x0f; // This counter is only 4 bits wide
            }
        }
    }
}