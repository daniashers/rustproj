use dsp::Dsp;
use std::fmt;
use std::cell::Cell;
use std::cell::RefCell;
use timer::Timer;
use clock::Clockable;
use std::rc::Rc;
use util::*;

pub type RAM = [u8; 65536];

pub trait Addressable {
    fn read(&self, address: u16) -> u8;
    fn write(&self, address: u16, value: u8);
}

pub struct Bus {
    pub ram: Rc<RefCell<RAM>>,
    pub rom: &'static [u8; 64],
    pub dsp: Rc<RefCell<Dsp>>,
    pub timer0: RefCell<Timer>,
    pub timer1: RefCell<Timer>,
    pub timer2: RefCell<Timer>,
    pub internal_counter: Cell<u32>
}

impl Bus {
    fn is_rom_enabled(&self) -> bool {
        self.ram.borrow()[SPCREGS::CONTROL as usize].msb()
    }

    fn is_timer_enabled(&self, timer_no: u8) -> bool {
        if timer_no > 2 { panic!("Timer {} does not exist", timer_no); }
        self.ram.borrow()[SPCREGS::CONTROL as usize].test(timer_no)
    }
    // TODO correctly activate/deactivate timers through the CONTROL register!

    // This should be called at 1.024 MHz
    pub fn tick_timers(&self) {
        let counter_value = self.internal_counter.get();
        if (counter_value & 0x7f) == 0 { self.timer0.borrow_mut().tick(); } // 128x prescaler
        if (counter_value & 0x7f) == 0 { self.timer1.borrow_mut().tick(); } // 128x prescaler
        if (counter_value & 0x0f) == 0 { self.timer2.borrow_mut().tick(); } // 16x prescaler
        self.internal_counter.set(counter_value.wrapping_add(1));
    }

    #[inline]
    fn is_system_register(addr: u16) -> bool {
        addr >= 0x00f0 && addr <= 0x00ff
    }

    // These functions are mainly used for display
    pub fn timer_0_period_nanos(&self) -> u64 {
        let mut target: u64 = self.timer0.borrow().programmable_value as u64;
        if target == 0 { target = 0x100; }
        1_000_000_000 * target / (T0FREQHZ as u64)
    }
    pub fn timer_1_period_nanos(&self) -> u64 {
        let mut target: u64 = self.timer1.borrow().programmable_value as u64;
        if target == 0 { target = 0x100; }
        1_000_000_000 * target / (T1FREQHZ as u64)
    }
    pub fn timer_2_period_nanos(&self) -> u64 {
        let mut target: u64 = self.timer2.borrow().programmable_value as u64;
        if target == 0 { target = 0x100; }
        1_000_000_000 * target / (T2FREQHZ as u64)
    }
}

impl Addressable for Bus {
    fn read(&self, address: u16) -> u8 {
        match address {
            //SPCREGS::TEST     => { } We just read back the RAM value for now. The value shouldn't REALLY be in RAM but ok for now. TODO
            SPCREGS::DSPADDR  => (*(self.dsp)).borrow().register_select,
            SPCREGS::DSPDATA  => (*(self.dsp)).borrow().read_register(),
         // SPCREGS::CPUIO0   =>  \  Since we are not communicating with a host system, we want to read back the same CPU IO values
         // SPCREGS::CPUIO1   =>   | that were read when the dump of the state was taken. This is important as the code may
         // SPCREGS::CPUIO2   =>   | expect specific values here. The appropriate values would have been loaded into RAM
         // SPCREGS::CPUIO3   =>  /  when loading the file, so a normal read from RAM will give us the desired values.
            SPCREGS::T0TARGET => { 0 }, // Write-only
            SPCREGS::T1TARGET => { 0 }, // Write-only
            SPCREGS::T2TARGET => { 0 }, // Write-only
            SPCREGS::T0OUT     => { self.timer0.borrow_mut().read_counter() },
            SPCREGS::T1OUT     => { self.timer1.borrow_mut().read_counter() },
            SPCREGS::T2OUT     => { self.timer2.borrow_mut().read_counter() },
            addr if (addr >= 0xffc0) && self.is_rom_enabled() => { self.rom[(addr & 0x003f) as usize]},
            _ => { let deref_ram = self.ram.borrow(); deref_ram[address as usize] }
        }
    }

    fn write(&self, address: u16, value: u8) {
        if Bus::is_system_register(address) {
            match address {
                SPCREGS::TEST     => { } // We don't allow changes to the Test register. TODO should we?
                SPCREGS::CONTROL  => {
                    self.timer0.borrow_mut().active = value.test(0);
                    self.timer1.borrow_mut().active = value.test(1);
                    self.timer2.borrow_mut().active = value.test(2);
                    // TODO what else is in the CONTROL register?
                    (*(self.ram)).borrow_mut()[SPCREGS::CONTROL as usize] = value;
                }
                SPCREGS::DSPADDR  => { (*(self.dsp)).borrow_mut().register_select = value; }
                SPCREGS::DSPDATA  => { (*(self.dsp)).borrow_mut().write_register(value); }
                SPCREGS::CPUIO0   => { }, // \
                SPCREGS::CPUIO1   => { }, //  | Since we are not communicating with a host system,
                SPCREGS::CPUIO2   => { }, //  | writes here will have no effect
                SPCREGS::CPUIO3   => { }, // /
                SPCREGS::T0TARGET => { self.timer0.borrow_mut().programmable_value = value; },
                SPCREGS::T1TARGET => { self.timer1.borrow_mut().programmable_value = value; },
                SPCREGS::T2TARGET => { self.timer2.borrow_mut().programmable_value = value; },
                SPCREGS::T0OUT    => { }, // Read-only
                SPCREGS::T1OUT    => { }, // Read-only
                SPCREGS::T2OUT    => { }, // Read-only
                _ => { }
            }
        } else {
            // write to normal RAM
            let mut deref_ram = (*(self.ram)).borrow_mut();
            deref_ram[address as usize] = value;
        }
    }
}

///// FakeBus can be used for testing purposes
//pub struct FakeBus {
//    pub read_value: u8,
//    pub events: Vec<Event>,
//}
//
//impl Addressable for FakeBus {
//    fn read(&self, address: u16) -> u8 {
//        self.events.push(Event::Read(address));
//        self.read_value
//    }
//
//    fn write(&self, address: u16, value: u8) {
//        self.events.push(Event::Write(address, value));
//    }
//}
//
//impl FakeBus {
//    pub fn new(read_value: u8) -> FakeBus {
//        FakeBus { read_value: read_value, events: Vec::new() }
//    }
//}
//
//#[derive(Debug)]
//pub enum Event {
//    Read(u16),
//    Write(u16, u8)
//}
//
//impl fmt::Display for Event {
//    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//        match *self {
//            Event::Read(addr)         => write!(f, "Read(0x{:04x})", addr),
//            Event::Write(addr, value) => write!(f, "Write(0x{:04x}, 0x{:02x})", addr, value)
//        }
//    }
//}

const T0FREQHZ: usize = 8000;
const T1FREQHZ: usize = 8000;
const T2FREQHZ: usize = 64000;

struct SPCREGS {}
impl SPCREGS {
    const TEST     : u16 = 0x00f0; // -w - Testing functions
    const CONTROL  : u16 = 0x00f1; // -w - I/O and Timer Control
    const DSPADDR  : u16 = 0x00f2; // rw - DSP Communication Address
    const DSPDATA  : u16 = 0x00f3; // rw - DSP Communication Data
    const CPUIO0   : u16 = 0x00f4;
    const CPUIO1   : u16 = 0x00f5;
    const CPUIO2   : u16 = 0x00f6;
    const CPUIO3   : u16 = 0x00f7;
    const T0TARGET : u16 = 0x00fa; // -w - Timer 0 Scaling Target
    const T1TARGET : u16 = 0x00fb; // -w - Timer 1 Scaling Target
    const T2TARGET : u16 = 0x00fc; // -w - Timer 2 Scaling Target
    const T0OUT    : u16 = 0x00fd; // r- - Timer 0 Output
    const T1OUT    : u16 = 0x00fe; // r- - Timer 1 Output
    const T2OUT    : u16 = 0x00ff; // r- - Timer 2 Output
}