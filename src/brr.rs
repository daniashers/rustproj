use util::*;
use bus::RAM;

pub struct Brr { // Note that this is stateful. In order to decode the next block, some samples of the old decoded block are used.
    raw_block: [u8; 9],
    decoded_sample_buffer: Vec<i16>,

    is_end_reached: bool,
    
    // This is basically just the sample position we're at, but with higher resolution (0x1000 for every sample) to allow for interpolation
    interpolation_index: u32,

    start_pointer: u16,
    loop_pointer: u16,
    current_pointer: u16,
}

impl Brr {
    pub fn new() -> Brr {
        Brr {
            raw_block: [0u8; 9],
            decoded_sample_buffer: vec!(0, 0, 0, 0), // we must 'seed' the buffer with something to allow the decoding to work
            is_end_reached: false,
            interpolation_index: 0,
            start_pointer: 0,
            loop_pointer: 0,
            current_pointer: 0,
        }
    }

    pub fn current_pointer(&self) -> u16 { self.current_pointer }

    pub fn is_end_reached(&self) -> bool { self.is_end_reached }
    pub fn is_terminate_immediately(&self) -> bool { self.end_flag() && !self.loop_flag() }

    pub fn start(&mut self, start_pointer: u16, loop_pointer: u16) {
        self.interpolation_index = 0;
        self.is_end_reached = false;
        self.start_pointer = start_pointer;
        self.loop_pointer = loop_pointer;
        self.current_pointer = start_pointer;
        self.decoded_sample_buffer = vec!(0, 0, 0, 0);

        self.raw_block = BLANK_BLOCK;
    }

    pub fn next_sample(&mut self, units_since_previous: u16, ram: &mut RAM) -> i16 {
        self.interpolation_index += units_since_previous as u32;
        if self.decoded_sample_buffer.len() <= 8 {
            // A new raw block is added to the buffer once there are less than 8 samples
            // remaining in the buffer. This most closely mimics the hardware timing.
            let next_raw_block = self.read_next_raw_block(ram);
            self.decode_next_block(next_raw_block);
        }
        if self.interpolation_index > 0x3fff {
            self.decoded_sample_buffer = self.decoded_sample_buffer.drop(4);
            self.interpolation_index -= 0x4000;
            // Old samples are dropped from the buffer every 4 samples used.
            // This most closely mimics the behaviour of the hardware and the timing at which
            // a new raw block is read.
        }

        let sample_interpolation = true;

        if sample_interpolation {
            // 4-point gaussian interpolation. These formulas have been used as-is.
            let sample_nr_within_block = (self.interpolation_index >> 12) as usize; // 0 <= i <= 7
            let one_256ths_of_sample = ((self.interpolation_index >> 4) & 0xff) as usize; // 0 <= d <= 255
            let s0 = self.decoded_sample_buffer[sample_nr_within_block];
            let s1 = self.decoded_sample_buffer[sample_nr_within_block + 1];
            let s2 = self.decoded_sample_buffer[sample_nr_within_block + 2];
            let s3 = self.decoded_sample_buffer[sample_nr_within_block + 3];
            let mut final_value: i32 =
                ((GAUSS_TABLE[255 - one_256ths_of_sample] * s0 as i32) >> 11) +
                ((GAUSS_TABLE[511 - one_256ths_of_sample] * s1 as i32) >> 11) +
                ((GAUSS_TABLE[256 + one_256ths_of_sample] * s2 as i32) >> 11) +
                ((GAUSS_TABLE[0   + one_256ths_of_sample] * s3 as i32) >> 11);
            if (final_value) > 32767 { final_value = 32767 };
            if (final_value) <= -32768 { final_value = -32768 };
            final_value as i16
        } else {
            self.decoded_sample_buffer[(self.interpolation_index >> 12) as usize]
        }

    }

    fn read_next_raw_block(&mut self, ram: &mut RAM) -> [u8; 9] {
        let mut block: [u8; 9] = [0; 9];

        if self.end_flag() {
            self.is_end_reached = true;
            self.current_pointer = self.loop_pointer;
        } else {
            self.current_pointer = self.current_pointer.wrapping_add(9);
        }

        let next_block_start = self.current_pointer;

        for i in 0..9 {
            block[i] = ram[next_block_start.wrapping_add(i as u16) as usize]
        }
        block
    }

    fn decode_next_block(&mut self, block: [u8; 9]) {
        self.raw_block = block;
        let shift = (block[0] >> 4) & 0x0f;

        // The 'filter' type to be used to decompress the samples
        let filter_nr: u8 = (block[0] >> 2) & 0x03;

        for n in 0..16 {
            let raw_sample: i8 = self.raw_sample_at(n);
            let unfiltered: i32 = if shift <= 12 {
                ((raw_sample as i32) << shift) >> 1 // These formulas are just to be taken as given!
            } else { // if shift > 12
                if raw_sample > 0 { 0 } else { -0x7800 } // Take this as given ('clipped' behaviour?) // TODO this may be wrong.
            };
            let prev1_sample = self.decoded_sample_buffer[self.decoded_sample_buffer.len() - 1] as i32; // We need to increase the precision to i32 temporarily
            let prev2_sample = self.decoded_sample_buffer[self.decoded_sample_buffer.len() - 2] as i32;
            let sample = match filter_nr {
                0 => unfiltered,
                1 => unfiltered + prev1_sample + ((-prev1_sample) >> 4),
                2 => unfiltered + (prev1_sample << 1) + ((-((prev1_sample << 1) + prev1_sample)) >> 5) - prev2_sample + (prev2_sample >> 4),
                3 => unfiltered + (prev1_sample << 1) + ((-(prev1_sample + (prev1_sample << 2) + (prev1_sample << 3))) >> 6) - prev2_sample + (((prev2_sample << 1) + prev2_sample) >> 4),
                _ => panic!("Not possible")
            };
            let clamped: i16 =
                if sample > (i16::max_value() as i32) { i16::max_value() }
                else if sample < (i16::min_value() as i32) { i16::min_value() }
                else { sample as i16 };
            self.decoded_sample_buffer.push(clamped); // TODO what about the 'clipped to 15 bits'?
        }
    }

    fn raw_sample_at(&self, pos: u8) -> i8 { // pos is 0..15, output is a signed number in the range -8..7
        let byte = self.raw_block[((pos >> 1) as usize) + 1];
        let nybble = if (pos % 2) != 0 { byte & 0x0f } else { (byte >> 4) & 0x0f };
        NYBBLE_TO_SIGNED[nybble as usize]
    }

    fn end_flag(&self) -> bool { self.raw_block[0].test(0) } // Whether the 'end' flag is set for this block
    fn loop_flag(&self) -> bool { self.raw_block[0].test(1) } // Whether the 'loop' flag is set for this block

}

// A convenience table to convert a raw nybble into a signed value
const NYBBLE_TO_SIGNED: [i8; 16] = [0, 1, 2, 3, 4, 5, 6, 7, -8, -7, -6, -5, -4, -3, -2, -1];
const BLANK_BLOCK: [u8; 9] = [0, 0, 0, 0, 0, 0, 0, 0, 0];

// credit libopenspc
const GAUSS_TABLE: [i32; 512] = [
    0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000,
    0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000,
    0x001, 0x001, 0x001, 0x001, 0x001, 0x001, 0x001, 0x001,
    0x001, 0x001, 0x001, 0x002, 0x002, 0x002, 0x002, 0x002,
    0x002, 0x002, 0x003, 0x003, 0x003, 0x003, 0x003, 0x004,
    0x004, 0x004, 0x004, 0x004, 0x005, 0x005, 0x005, 0x005,
    0x006, 0x006, 0x006, 0x006, 0x007, 0x007, 0x007, 0x008,
    0x008, 0x008, 0x009, 0x009, 0x009, 0x00A, 0x00A, 0x00A,
    0x00B, 0x00B, 0x00B, 0x00C, 0x00C, 0x00D, 0x00D, 0x00E,
    0x00E, 0x00F, 0x00F, 0x00F, 0x010, 0x010, 0x011, 0x011,
    0x012, 0x013, 0x013, 0x014, 0x014, 0x015, 0x015, 0x016,
    0x017, 0x017, 0x018, 0x018, 0x019, 0x01A, 0x01B, 0x01B,
    0x01C, 0x01D, 0x01D, 0x01E, 0x01F, 0x020, 0x020, 0x021,
    0x022, 0x023, 0x024, 0x024, 0x025, 0x026, 0x027, 0x028,
    0x029, 0x02A, 0x02B, 0x02C, 0x02D, 0x02E, 0x02F, 0x030,
    0x031, 0x032, 0x033, 0x034, 0x035, 0x036, 0x037, 0x038,

    0x03A, 0x03B, 0x03C, 0x03D, 0x03E, 0x040, 0x041, 0x042,
    0x043, 0x045, 0x046, 0x047, 0x049, 0x04A, 0x04C, 0x04D,
    0x04E, 0x050, 0x051, 0x053, 0x054, 0x056, 0x057, 0x059,
    0x05A, 0x05C, 0x05E, 0x05F, 0x061, 0x063, 0x064, 0x066,
    0x068, 0x06A, 0x06B, 0x06D, 0x06F, 0x071, 0x073, 0x075,
    0x076, 0x078, 0x07A, 0x07C, 0x07E, 0x080, 0x082, 0x084,
    0x086, 0x089, 0x08B, 0x08D, 0x08F, 0x091, 0x093, 0x096,
    0x098, 0x09A, 0x09C, 0x09F, 0x0A1, 0x0A3, 0x0A6, 0x0A8,
    0x0AB, 0x0AD, 0x0AF, 0x0B2, 0x0B4, 0x0B7, 0x0BA, 0x0BC,
    0x0BF, 0x0C1, 0x0C4, 0x0C7, 0x0C9, 0x0CC, 0x0CF, 0x0D2,
    0x0D4, 0x0D7, 0x0DA, 0x0DD, 0x0E0, 0x0E3, 0x0E6, 0x0E9,
    0x0EC, 0x0EF, 0x0F2, 0x0F5, 0x0F8, 0x0FB, 0x0FE, 0x101,
    0x104, 0x107, 0x10B, 0x10E, 0x111, 0x114, 0x118, 0x11B,
    0x11E, 0x122, 0x125, 0x129, 0x12C, 0x130, 0x133, 0x137,
    0x13A, 0x13E, 0x141, 0x145, 0x148, 0x14C, 0x150, 0x153,
    0x157, 0x15B, 0x15F, 0x162, 0x166, 0x16A, 0x16E, 0x172,

    0x176, 0x17A, 0x17D, 0x181, 0x185, 0x189, 0x18D, 0x191,
    0x195, 0x19A, 0x19E, 0x1A2, 0x1A6, 0x1AA, 0x1AE, 0x1B2,
    0x1B7, 0x1BB, 0x1BF, 0x1C3, 0x1C8, 0x1CC, 0x1D0, 0x1D5,
    0x1D9, 0x1DD, 0x1E2, 0x1E6, 0x1EB, 0x1EF, 0x1F3, 0x1F8,
    0x1FC, 0x201, 0x205, 0x20A, 0x20F, 0x213, 0x218, 0x21C,
    0x221, 0x226, 0x22A, 0x22F, 0x233, 0x238, 0x23D, 0x241,
    0x246, 0x24B, 0x250, 0x254, 0x259, 0x25E, 0x263, 0x267,
    0x26C, 0x271, 0x276, 0x27B, 0x280, 0x284, 0x289, 0x28E,
    0x293, 0x298, 0x29D, 0x2A2, 0x2A6, 0x2AB, 0x2B0, 0x2B5,
    0x2BA, 0x2BF, 0x2C4, 0x2C9, 0x2CE, 0x2D3, 0x2D8, 0x2DC,
    0x2E1, 0x2E6, 0x2EB, 0x2F0, 0x2F5, 0x2FA, 0x2FF, 0x304,
    0x309, 0x30E, 0x313, 0x318, 0x31D, 0x322, 0x326, 0x32B,
    0x330, 0x335, 0x33A, 0x33F, 0x344, 0x349, 0x34E, 0x353,
    0x357, 0x35C, 0x361, 0x366, 0x36B, 0x370, 0x374, 0x379,
    0x37E, 0x383, 0x388, 0x38C, 0x391, 0x396, 0x39B, 0x39F,

    0x3A4, 0x3A9, 0x3AD, 0x3B2, 0x3B7, 0x3BB, 0x3C0, 0x3C5,
    0x3C9, 0x3CE, 0x3D2, 0x3D7, 0x3DC, 0x3E0, 0x3E5, 0x3E9,
    0x3ED, 0x3F2, 0x3F6, 0x3FB, 0x3FF, 0x403, 0x408, 0x40C,
    0x410, 0x415, 0x419, 0x41D, 0x421, 0x425, 0x42A, 0x42E,
    0x432, 0x436, 0x43A, 0x43E, 0x442, 0x446, 0x44A, 0x44E,
    0x452, 0x455, 0x459, 0x45D, 0x461, 0x465, 0x468, 0x46C,
    0x470, 0x473, 0x477, 0x47A, 0x47E, 0x481, 0x485, 0x488,
    0x48C, 0x48F, 0x492, 0x496, 0x499, 0x49C, 0x49F, 0x4A2,
    0x4A6, 0x4A9, 0x4AC, 0x4AF, 0x4B2, 0x4B5, 0x4B7, 0x4BA,
    0x4BD, 0x4C0, 0x4C3, 0x4C5, 0x4C8, 0x4CB, 0x4CD, 0x4D0,
    0x4D2, 0x4D5, 0x4D7, 0x4D9, 0x4DC, 0x4DE, 0x4E0, 0x4E3,
    0x4E5, 0x4E7, 0x4E9, 0x4EB, 0x4ED, 0x4EF, 0x4F1, 0x4F3,
    0x4F5, 0x4F6, 0x4F8, 0x4FA, 0x4FB, 0x4FD, 0x4FF, 0x500,
    0x502, 0x503, 0x504, 0x506, 0x507, 0x508, 0x50A, 0x50B,
    0x50C, 0x50D, 0x50E, 0x50F, 0x510, 0x511, 0x511, 0x512,
    0x513, 0x514, 0x514, 0x515, 0x516, 0x516, 0x517, 0x517,
    0x517, 0x518, 0x518, 0x518, 0x518, 0x518, 0x519, 0x519
];
