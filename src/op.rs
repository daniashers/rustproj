#[allow(bad_style)] // Suppress warnings suggesting changing the op names to camel case. It would be less readable.
#[derive(Debug)]
pub enum Instruction {
    Mov_A_Imm,
    Mov_A_IndX,
    Mov_A_IndXInc,
    Mov_A_Dp,
    Mov_A_DpIdxX,
    Mov_A_Abs,
    Mov_A_AbsIdxX,
    Mov_A_AbsIdxY,
    Mov_A_Ind_DpIdxX,
    Mov_A_IndDp_IdxY,
    Mov_X_Imm,
    Mov_X_Dp,
    Mov_X_DpIdxY,
    Mov_X_Abs,
    Mov_Y_Imm,
    Mov_Y_Dp,
    Mov_Y_DpIdxX,
    Mov_Y_Abs,
    Mov_IndX_A,
    Mov_IndXInc_A,
    Mov_Dp_A,
    Mov_DpIdxX_A,
    Mov_Abs_A,
    Mov_AbsIdxX_A,
    Mov_AbsIdxY_A,
    Mov_Ind_DpIdxX_A,
    Mov_IndDp_IdxY_A,
    Mov_Dp_X,
    Mov_DpIdxY_X,
    Mov_Abs_X,
    Mov_Dp_Y,
    Mov_DpIdxX_Y,
    Mov_Abs_Y,
    Mov_A_X,
    Mov_A_Y,
    Mov_X_A,
    Mov_Y_A,
    Mov_X_SP,
    Mov_SP_X,
    Mov_Dp_Dp,
    Mov_Dp_Imm,
    Adc_A_Imm,
    Adc_A_IndX,
    Adc_A_Dp,
    Adc_A_DpIdxX,
    Adc_A_Abs,
    Adc_A_AbsIdxX,
    Adc_A_AbsIdxY,
    Adc_A_Ind_DpIdxX,
    Adc_A_IndDp_IdxY,
    Adc_IndX_IndY,
    Adc_Dp_Dp,
    Adc_Dp_Imm,
    Sbc_A_Imm,
    Sbc_A_IndX,
    Sbc_A_Dp,
    Sbc_A_DpIdxX,
    Sbc_A_Abs,
    Sbc_A_AbsIdxX,
    Sbc_A_AbsIdxY,
    Sbc_A_Ind_DpIdxX,
    Sbc_A_IndDp_IdxY,
    Sbc_IndX_IndY,
    Sbc_Dp_Dp,
    Sbc_Dp_Imm,
    Cmp_A_Imm,
    Cmp_A_IndX,
    Cmp_A_Dp,
    Cmp_A_DpIdxX,
    Cmp_A_Abs,
    Cmp_A_AbsIdxX,
    Cmp_A_AbsIdxY,
    Cmp_A_Ind_DpIdxX,
    Cmp_A_IndDp_IdxY,
    Cmp_IndX_IndY,
    Cmp_Dp_Dp,
    Cmp_Dp_Imm,
    Cmp_X_Imm,
    Cmp_X_Dp,
    Cmp_X_Abs,
    Cmp_Y_Imm,
    Cmp_Y_Dp,
    Cmp_Y_Abs,
    And_A_Imm,
    And_A_IndX,
    And_A_Dp,
    And_A_DpIdxX,
    And_A_Abs,
    And_A_AbsIdxX,
    And_A_AbsIdxY,
    And_A_Ind_DpIdxX,
    And_A_IndDp_IdxY,
    And_IndX_IndY,
    And_Dp_Dp,
    And_Dp_Imm,
    Or_A_Imm,
    Or_A_IndX,
    Or_A_Dp,
    Or_A_DpIdxX,
    Or_A_Abs,
    Or_A_AbsIdxX,
    Or_A_AbsIdxY,
    Or_A_Ind_DpIdxX,
    Or_A_IndDp_IdxY,
    Or_IndX_IndY,
    Or_Dp_Dp,
    Or_Dp_Imm,
    Eor_A_Imm,
    Eor_A_IndX,
    Eor_A_Dp,
    Eor_A_DpIdxX,
    Eor_A_Abs,
    Eor_A_AbsIdxX,
    Eor_A_AbsIdxY,
    Eor_A_Ind_DpIdxX,
    Eor_A_IndDp_IdxY,
    Eor_IndX_IndY,
    Eor_Dp_Dp,
    Eor_Dp_Imm,
    Inc_A,
    Inc_Dp,
    Inc_DpIdxX,
    Inc_Abs,
    Inc_X,
    Inc_Y,
    Dec_A,
    Dec_Dp,
    Dec_DpIdxX,
    Dec_Abs,
    Dec_X,
    Dec_Y,
    Asl_A,
    Asl_Dp,
    Asl_DpIdxX,
    Asl_Abs,
    Lsr_A,
    Lsr_Dp,
    Lsr_DpIdxX,
    Lsr_Abs,
    Rol_A,
    Rol_Dp,
    Rol_DpIdxX,
    Rol_Abs,
    Ror_A,
    Ror_Dp,
    Ror_DpIdxX,
    Ror_Abs,
    Xcn_A,
    Movw_YA_Dp,
    Movw_Dp_YA,
    Incw_Dp,
    Decw_Dp,
    Addw_YA_Dp,
    Subw_YA_Dp,
    Cmpw_YA_Dp,
    Mul_YA,
    Div_YA_X,
    Daa_A,
    Das_A,
    Bra,
    Beq,
    Bne,
    Bcs,
    Bcc,
    Bvs,
    Bvc,
    Bmi,
    Bpl,
    Bbs0_Dp,
    Bbs1_Dp,
    Bbs2_Dp,
    Bbs3_Dp,
    Bbs4_Dp,
    Bbs5_Dp,
    Bbs6_Dp,
    Bbs7_Dp,
    Bbc0_Dp,
    Bbc1_Dp,
    Bbc2_Dp,
    Bbc3_Dp,
    Bbc4_Dp,
    Bbc5_Dp,
    Bbc6_Dp,
    Bbc7_Dp,
    Cbne_Dp,
    Cbne_DpIdxX,
    Dbnz_Dp,
    Dbnz_Y,
    Jmp,
    Jmp_Ind_AbsIdxX,
    Call,
    Pcall,
    Tcall0,
    Tcall1,
    Tcall2,
    Tcall3,
    Tcall4,
    Tcall5,
    Tcall6,
    Tcall7,
    Tcall8,
    Tcall9,
    TcallA,
    TcallB,
    TcallC,
    TcallD,
    TcallE,
    TcallF,
    Brk,
    Ret,
    Ret1,
    Push_A,
    Push_X,
    Push_Y,
    Push_Psw,
    Pop_A,
    Pop_X,
    Pop_Y,
    Pop_Psw,
    Set0_Dp,
    Set1_Dp,
    Set2_Dp,
    Set3_Dp,
    Set4_Dp,
    Set5_Dp,
    Set6_Dp,
    Set7_Dp,
    Clr0_Dp,
    Clr1_Dp,
    Clr2_Dp,
    Clr3_Dp,
    Clr4_Dp,
    Clr5_Dp,
    Clr6_Dp,
    Clr7_Dp,
    Tset1,
    Tclr1,
    And1_C_Mem,
    And1_C_NotMem,
    Or1_C_Mem,
    Or1_C_NotMem,
    Eor1_C_Mem,
    Not1_Mem,
    Mov1_C_Mem,
    Mov1_Mem_C,
    Clrc,
    Setc,
    Notc,
    Clrv,
    Clrp,
    Setp,
    Ei,
    Di,
    Nop,
    Sleep,
    Stop,
}

impl Instruction {

    pub fn from_opcode(opcode: u8) -> &'static Instruction {
        &OPCODE_TABLE[opcode as usize]
    }
}

use self::Instruction::*;

static OPCODE_TABLE: [Instruction; 256] =
    [
        Nop,          Tcall0,        Set0_Dp,       Bbs0_Dp,
        Or_A_Dp,      Or_A_Abs,      Or_A_IndX,     Or_A_Ind_DpIdxX,
        Or_A_Imm,     Or_Dp_Dp,      Or1_C_Mem,     Asl_Dp,
        Asl_Abs,      Push_Psw,      Tset1,         Brk,

        Bpl,          Tcall1,        Clr0_Dp,       Bbc0_Dp,
        Or_A_DpIdxX,  Or_A_AbsIdxX,  Or_A_AbsIdxY,  Or_A_IndDp_IdxY,
        Or_Dp_Imm,    Or_IndX_IndY,  Decw_Dp,       Asl_DpIdxX,
        Asl_A,        Dec_X,         Cmp_X_Abs,     Jmp_Ind_AbsIdxX,

        Clrp,         Tcall2,        Set1_Dp,       Bbs1_Dp,
        And_A_Dp,     And_A_Abs,     And_A_IndX,    And_A_Ind_DpIdxX,
        And_A_Imm,    And_Dp_Dp,     Or1_C_NotMem,  Rol_Dp,
        Rol_Abs,      Push_A,        Cbne_Dp,       Bra,

        Bmi,          Tcall3,        Clr1_Dp,       Bbc1_Dp,
        And_A_DpIdxX, And_A_AbsIdxX, And_A_AbsIdxY, And_A_IndDp_IdxY,
        And_Dp_Imm,   And_IndX_IndY, Incw_Dp,       Rol_DpIdxX,
        Rol_A,        Inc_X,         Cmp_X_Dp,      Call,

        Setp,         Tcall4,        Set2_Dp,       Bbs2_Dp,
        Eor_A_Dp,     Eor_A_Abs,     Eor_A_IndX,    Eor_A_Ind_DpIdxX,
        Eor_A_Imm,    Eor_Dp_Dp,     And1_C_Mem,    Lsr_Dp,
        Lsr_Abs,      Push_X,        Tclr1,         Pcall,

        Bvc,          Tcall5,        Clr2_Dp,       Bbc2_Dp,
        Eor_A_DpIdxX, Eor_A_AbsIdxX, Eor_A_AbsIdxY, Eor_A_IndDp_IdxY,
        Eor_Dp_Imm,   Eor_IndX_IndY, Cmpw_YA_Dp,    Lsr_DpIdxX,
        Lsr_A,        Mov_X_A,       Cmp_Y_Abs,     Jmp,

        Clrc,         Tcall6,        Set3_Dp,       Bbs3_Dp,
        Cmp_A_Dp,     Cmp_A_Abs,     Cmp_A_IndX,    Cmp_A_Ind_DpIdxX,
        Cmp_A_Imm,    Cmp_Dp_Dp,     And1_C_NotMem, Ror_Dp,
        Ror_Abs,      Push_Y,        Dbnz_Dp,       Ret,

        Bvs,          Tcall7,        Clr3_Dp,       Bbc3_Dp,
        Cmp_A_DpIdxX, Cmp_A_AbsIdxX, Cmp_A_AbsIdxY, Cmp_A_IndDp_IdxY,
        Cmp_Dp_Imm,   Cmp_IndX_IndY, Addw_YA_Dp,    Ror_DpIdxX,
        Ror_A,        Mov_A_X,       Cmp_Y_Dp,      Ret1,

        Setc,         Tcall8,        Set4_Dp,       Bbs4_Dp,
        Adc_A_Dp,     Adc_A_Abs,     Adc_A_IndX,    Adc_A_Ind_DpIdxX,
        Adc_A_Imm,    Adc_Dp_Dp,     Eor1_C_Mem,    Dec_Dp,
        Dec_Abs,      Mov_Y_Imm,     Pop_Psw,       Mov_Dp_Imm,

        Bcc,          Tcall9,        Clr4_Dp,       Bbc4_Dp,
        Adc_A_DpIdxX, Adc_A_AbsIdxX, Adc_A_AbsIdxY, Adc_A_IndDp_IdxY,
        Adc_Dp_Imm,   Adc_IndX_IndY, Subw_YA_Dp,    Dec_DpIdxX,
        Dec_A,        Mov_X_SP,      Div_YA_X,      Xcn_A,

        Ei,           TcallA,        Set5_Dp,       Bbs5_Dp,
        Sbc_A_Dp,     Sbc_A_Abs,     Sbc_A_IndX,    Sbc_A_Ind_DpIdxX,
        Sbc_A_Imm,    Sbc_Dp_Dp,     Mov1_C_Mem,    Inc_Dp,
        Inc_Abs,      Cmp_Y_Imm,     Pop_A,         Mov_IndXInc_A,

        Bcs,          TcallB,        Clr5_Dp,       Bbc5_Dp,
        Sbc_A_DpIdxX, Sbc_A_AbsIdxX, Sbc_A_AbsIdxY, Sbc_A_IndDp_IdxY,
        Sbc_Dp_Imm,   Sbc_IndX_IndY, Movw_YA_Dp,    Inc_DpIdxX,
        Inc_A,        Mov_SP_X,      Das_A,         Mov_A_IndXInc,

        Di,           TcallC,        Set6_Dp,       Bbs6_Dp,
        Mov_Dp_A,     Mov_Abs_A,     Mov_IndX_A,    Mov_Ind_DpIdxX_A,
        Cmp_X_Imm,    Mov_Abs_X,     Mov1_Mem_C,    Mov_Dp_Y,
        Mov_Abs_Y,    Mov_X_Imm,     Pop_X,         Mul_YA,

        Bne,          TcallD,        Clr6_Dp,       Bbc6_Dp,
        Mov_DpIdxX_A, Mov_AbsIdxX_A, Mov_AbsIdxY_A, Mov_IndDp_IdxY_A,
        Mov_Dp_X,     Mov_DpIdxY_X,  Movw_Dp_YA,    Mov_DpIdxX_Y,
        Dec_Y,        Mov_A_Y,       Cbne_DpIdxX,   Daa_A,

        Clrv,         TcallE,        Set7_Dp,       Bbs7_Dp,
        Mov_A_Dp,     Mov_A_Abs,     Mov_A_IndX,    Mov_A_Ind_DpIdxX,
        Mov_A_Imm,    Mov_X_Abs,     Not1_Mem,      Mov_Y_Dp,
        Mov_Y_Abs,    Notc,          Pop_Y,         Sleep,

        Beq,          TcallF,        Clr7_Dp,       Bbc7_Dp,
        Mov_A_DpIdxX, Mov_A_AbsIdxX, Mov_A_AbsIdxY, Mov_A_IndDp_IdxY,
        Mov_X_Dp,     Mov_X_DpIdxY,  Mov_Dp_Dp,     Mov_Y_DpIdxX,
        Inc_Y,        Mov_Y_A,       Dbnz_Y,        Stop
        ];

pub static INSTR_LENGTHS: [i8; 256] = [
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 1,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 3, 3,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3,-2,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 2, 3,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 2,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 3, 3,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 1,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 2, 1,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 2, 1, 3,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 1, 1,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 2, 1, 1,
   -2, 1, 2, 3, 2, 3, 3, 2, 3, 1, 2, 2, 1, 1, 1, 1,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 2, 1, 1,
   -2, 1, 2, 3, 2, 3, 3, 2, 2, 2, 2, 2, 1, 1, 3, 1,
    1, 1, 2, 3, 2, 3, 1, 2, 2, 3, 3, 2, 3, 1, 1, 1,
   -2, 1, 2, 3, 2, 3, 3, 2, 2, 2, 3, 2, 1, 1, 2, 1
];


/*
  E8, Mov_A_Imm         , 2
  E6, Mov_A_IndX        , 1
  BF, Mov_A_IndXInc     , 1
  E4, Mov_A_Dp          , 2
  F4, Mov_A_DpIdxX      , 2
  E5, Mov_A_Abs         , 3
  F5, Mov_A_AbsIdxX     , 3
  F6, Mov_A_AbsIdxY     , 3
  E7, Mov_A_Ind_DpIdxX  , 2
  F7, Mov_A_IndDp_IdxY  , 2
  CD, Mov_X_Imm         , 2
  F8, Mov_X_Dp          , 2
  F9, Mov_X_DpIdxY      , 2
  E9, Mov_X_Abs         , 3
  8D, Mov_Y_Imm         , 2
  EB, Mov_Y_Dp          , 2
  FB, Mov_Y_DpIdxX      , 2
  EC, Mov_Y_Abs         , 3
  C6, Mov_IndX_A        , 1
  AF, Mov_IndXInc_A     , 1
  C4, Mov_Dp_A          , 2
  D4, Mov_DpIdxX_A      , 2
  C5, Mov_Abs_A         , 3
  D5, Mov_AbsIdxX_A     , 3
  D6, Mov_AbsIdxY_A     , 3
  C7, Mov_Ind_DpIdxX_A  , 2
  D7, Mov_IndDp_IdxY_A  , 2
  D8, Mov_Dp_X          , 2
  D9, Mov_DpIdxY_X      , 2
  C9, Mov_Abs_X         , 3
  CB, Mov_Dp_Y          , 2
  DB, Mov_DpIdxX_Y      , 2
  CC, Mov_Abs_Y         , 3
  7D, Mov_A_X           , 1
  DD, Mov_A_Y           , 1
  5D, Mov_X_A           , 1
  FD, Mov_Y_A           , 1
  9D, Mov_X_SP          , 1
  BD, Mov_SP_X          , 1
  FA, Mov_Dp_Dp         , 3
  8F, Mov_Dp_Imm        , 3
  88, Adc_A_Imm         , 2
  86, Adc_A_IndX        , 1
  84, Adc_A_Dp          , 2
  94, Adc_A_DpIdxX      , 2
  85, Adc_A_Abs         , 3
  95, Adc_A_AbsIdxX     , 3
  96, Adc_A_AbsIdxY     , 3
  87, Adc_A_Ind_DpIdxX  , 2
  97, Adc_A_IndDp_IdxY  , 2
  99, Adc_IndX_IndY     , 1
  89, Adc_Dp_Dp         , 3
  98, Adc_Dp_Imm        , 3
  A8, Sbc_A_Imm         , 2
  A6, Sbc_A_IndX        , 1
  A4, Sbc_A_Dp          , 2
  B4, Sbc_A_DpIdxX      , 2
  A5, Sbc_A_Abs         , 3
  B5, Sbc_A_AbsIdxX     , 3
  B6, Sbc_A_AbsIdxY     , 3
  A7, Sbc_A_Ind_DpIdxX  , 2
  B7, Sbc_A_IndDp_IdxY  , 2
  B9, Sbc_IndX_IndY     , 1
  A9, Sbc_Dp_Dp         , 3
  B8, Sbc_Dp_Imm        , 3
  68, Cmp_A_Imm         , 2
  66, Cmp_A_IndX        , 1
  64, Cmp_A_Dp          , 2
  74, Cmp_A_DpIdxX      , 2
  65, Cmp_A_Abs         , 3
  75, Cmp_A_AbsIdxX     , 3
  76, Cmp_A_AbsIdxY     , 3
  67, Cmp_A_Ind_DpIdxX  , 2
  77, Cmp_A_IndDp_IdxY  , 2
  79, Cmp_IndX_IndY     , 1
  69, Cmp_Dp_Dp         , 3
  78, Cmp_Dp_Imm        , 3
  C8, Cmp_X_Imm         , 2
  3E, Cmp_X_Dp          , 2
  1E, Cmp_X_Abs         , 3
  AD, Cmp_Y_Imm         , 2
  7E, Cmp_Y_Dp          , 2
  5E, Cmp_Y_Abs         , 3
  28, And_A_Imm         , 2
  26, And_A_IndX        , 1
  24, And_A_Dp          , 2
  34, And_A_DpIdxX      , 2
  25, And_A_Abs         , 3
  35, And_A_AbsIdxX     , 3
  36, And_A_AbsIdxY     , 3
  27, And_A_Ind_DpIdxX  , 2
  37, And_A_IndDp_IdxY  , 2
  39, And_IndX_IndY     , 1
  29, And_Dp_Dp         , 3
  38, And_Dp_Imm        , 3
  08, Or_A_Imm          , 2
  06, Or_A_IndX         , 1
  04, Or_A_Dp           , 2
  14, Or_A_DpIdxX       , 2
  05, Or_A_Abs          , 3
  15, Or_A_AbsIdxX      , 3
  16, Or_A_AbsIdxY      , 3
  07, Or_A_Ind_DpIdxX   , 2
  17, Or_A_IndDp_IdxY   , 2
  19, Or_IndX_IndY      , 1
  09, Or_Dp_Dp          , 3
  18, Or_Dp_Imm         , 3
  48, Eor_A_Imm         , 2
  46, Eor_A_IndX        , 1
  44, Eor_A_Dp          , 2
  54, Eor_A_DpIdxX      , 2
  45, Eor_A_Abs         , 3
  55, Eor_A_AbsIdxX     , 3
  56, Eor_A_AbsIdxY     , 3
  47, Eor_A_Ind_DpIdxX  , 2
  57, Eor_A_IndDp_IdxY  , 2
  59, Eor_IndX_IndY     , 1
  49, Eor_Dp_Dp         , 3
  58, Eor_Dp_Imm        , 3
  BC, Inc_A             , 1
  AB, Inc_Dp            , 2
  BB, Inc_DpIdxX        , 2
  AC, Inc_Abs           , 3
  3D, Inc_X             , 1
  FC, Inc_Y             , 1
  9C, Dec_A             , 1
  8B, Dec_Dp            , 2
  9B, Dec_DpIdxX        , 2
  8C, Dec_Abs           , 3
  1D, Dec_X             , 1
  DC, Dec_Y             , 1
  1C, Asl_A             , 1
  0B, Asl_Dp            , 2
  1B, Asl_DpIdxX        , 2
  0C, Asl_Abs           , 3
  5C, Lsr_A             , 1
  4B, Lsr_Dp            , 2
  5B, Lsr_DpIdxX        , 2
  4C, Lsr_Abs           , 3
  3C, Rol_A             , 1
  2B, Rol_Dp            , 2
  3B, Rol_DpIdxX        , 2
  2C, Rol_Abs           , 3
  7C, Ror_A             , 1
  6B, Ror_Dp            , 2
  7B, Ror_DpIdxX        , 2
  6C, Ror_Abs           , 3
  9F, Xcn_A             , 1
  BA, Movw_YA_Dp        , 2
  DA, Movw_Dp_YA        , 2
  3A, Incw_Dp           , 2
  1A, Decw_Dp           , 2
  7A, Addw_YA_Dp        , 2
  9A, Subw_YA_Dp        , 2
  5A, Cmpw_YA_Dp        , 2
  CF, Mul_YA            , 1
  9E, Div_YA_X          , 1
  DF, Daa_A             , 1
  BE, Das_A             , 1
  2F, Bra               , 2
  F0, Beq               , 2
  D0, Bne               , 2
  B0, Bcs               , 2
  90, Bcc               , 2
  70, Bvs               , 2
  50, Bvc               , 2
  30, Bmi               , 2
  10, Bpl               , 2
  03, Bbs0_Dp           , 3
  23, Bbs1_Dp           , 3
  43, Bbs2_Dp           , 3
  63, Bbs3_Dp           , 3
  83, Bbs4_Dp           , 3
  A3, Bbs5_Dp           , 3
  C3, Bbs6_Dp           , 3
  E3, Bbs7_Dp           , 3
  13, Bbc0_Dp           , 3
  33, Bbc1_Dp           , 3
  53, Bbc2_Dp           , 3
  73, Bbc3_Dp           , 3
  93, Bbc4_Dp           , 3
  B3, Bbc5_Dp           , 3
  D3, Bbc6_Dp           , 3
  F3, Bbc7_Dp           , 3
  2E, Cbne_Dp           , 3
  DE, Cbne_DpIdxX       , 3
  6E, Dbnz_Dp           , 3
  FE, Dbnz_Y            , 2
  5F, Jmp               , 3
  1F, Jmp_Ind_AbsIdxX   , 3
  3F, Call              , 3
  4F, Pcall             , 2
  01, Tcall0            , 1
  11, Tcall1            , 1
  21, Tcall2            , 1
  31, Tcall3            , 1
  41, Tcall4            , 1
  51, Tcall5            , 1
  61, Tcall6            , 1
  71, Tcall7            , 1
  81, Tcall8            , 1
  91, Tcall9            , 1
  A1, TcallA            , 1
  B1, TcallB            , 1
  C1, TcallC            , 1
  D1, TcallD            , 1
  E1, TcallE            , 1
  F1, TcallF            , 1
  0F, Brk               , 1
  6F, Ret               , 1
  7F, Ret1              , 1
  2D, Push_A            , 1
  4D, Push_X            , 1
  6D, Push_Y            , 1
  0D, Push_Psw          , 1
  AE, Pop_A             , 1
  CE, Pop_X             , 1
  EE, Pop_Y             , 1
  8E, Pop_Psw           , 1
  02, Set0_Dp           , 2
  22, Set1_Dp           , 2
  42, Set2_Dp           , 2
  62, Set3_Dp           , 2
  82, Set4_Dp           , 2
  A2, Set5_Dp           , 2
  C2, Set6_Dp           , 2
  E2, Set7_Dp           , 2
  12, Clr0_Dp           , 2
  32, Clr1_Dp           , 2
  52, Clr2_Dp           , 2
  72, Clr3_Dp           , 2
  92, Clr4_Dp           , 2
  B2, Clr5_Dp           , 2
  D2, Clr6_Dp           , 2
  F2, Clr7_Dp           , 2
  0E, Tset1             , 3
  4E, Tclr1             , 3
  4A, And1_C_Mem        , 3
  6A, And1_C_NotMem     , 3
  0A, Or1_C_Mem         , 3
  2A, Or1_C_NotMem      , 3
  8A, Eor1_C_Mem        , 3
  EA, Not1_Mem          , 3
  AA, Mov1_C_Mem        , 3
  CA, Mov1_Mem_C        , 3
  60, Clrc              , 1
  80, Setc              , 1
  ED, Notc              , 1
  E0, Clrv              , 1
  20, Clrp              , 1
  40, Setp              , 1
  A0, Ei                , 1
  C0, Di                , 1
  00, Nop               , 1
  EF, Sleep             , 1
  FF, Stop              , 1
*/

