use util::StereoSample;

/// A class to resample audio from 32000Hz to 44100Hz.
pub struct Upsample {
    buffer_32000: Vec<StereoSample>,
    buffer_44100: Vec<StereoSample>
}

impl Upsample {

    pub fn new() -> Upsample {
        Upsample {
            buffer_32000: vec!(),
            buffer_44100: vec!()
        }
    }

    pub fn feed(&mut self, samples: &[StereoSample]) {
        // Grow the buffer with the new input samples
        self.buffer_32000.extend_from_slice(samples);
        // Now process as many samples as possible and remove them from the buffer.
        while self.buffer_32000.len() >= 320 {
            for i in 0..441 {
                self.buffer_44100.push(self.buffer_32000[RESAMPLE_TABLE[i]]);
            }
            let buf32000_minus_first_320 = (&self.buffer_32000[320..]).to_vec();
            self.buffer_32000 = buf32000_minus_first_320; // drop the 'used' samples from the beginning of the buffer
        }

    }

    pub fn samples_available(&self) -> usize {
        self.buffer_44100.len()
    }

    pub fn gimme_n_samples(&mut self, n: usize) -> Vec<StereoSample> {
        if n > self.samples_available() {
            panic!("Requested {} samples but only {} available", n, self.samples_available());
        }
        let samples = (&self.buffer_44100[0..n]).to_vec(); // take n samples from the beginning of the buffer
        self.buffer_44100 = (&self.buffer_44100[n..]).to_vec(); // drop the 'used' samples from the beginning of the bugger
        samples
    }

}

// This is a table for a 'sample and hold' resampling implementation which is the simplest.
// 320 input samples become 441 output samples as per the indices below.
const RESAMPLE_TABLE: [usize; 441] = [
      0,   0,   1,   2,   2,   3,   4,   5,   5,   6,   7,   7,   8,   9,  10,  10,  11,  12,  13,  13,  14,
     15,  15,  16,  17,  18,  18,  19,  20,  21,  21,  22,  23,  23,  24,  25,  26,  26,  27,  28,  29,  29,
     30,  31,  31,  32,  33,  34,  34,  35,  36,  37,  37,  38,  39,  39,  40,  41,  42,  42,  43,  44,  44,
     45,  46,  47,  47,  48,  49,  50,  50,  51,  52,  52,  53,  54,  55,  55,  56,  57,  58,  58,  59,  60,
     60,  61,  62,  63,  63,  64,  65,  66,  66,  67,  68,  68,  69,  70,  71,  71,  72,  73,  74,  74,  75,
     76,  76,  77,  78,  79,  79,  80,  81,  81,  82,  83,  84,  84,  85,  86,  87,  87,  88,  89,  89,  90,
     91,  92,  92,  93,  94,  95,  95,  96,  97,  97,  98,  99, 100, 100, 101, 102, 103, 103, 104, 105, 105,
    106, 107, 108, 108, 109, 110, 111, 111, 112, 113, 113, 114, 115, 116, 116, 117, 118, 119, 119, 120, 121,
    121, 122, 123, 124, 124, 125, 126, 126, 127, 128, 129, 129, 130, 131, 132, 132, 133, 134, 134, 135, 136,
    137, 137, 138, 139, 140, 140, 141, 142, 142, 143, 144, 145, 145, 146, 147, 148, 148, 149, 150, 150, 151,
    152, 153, 153, 154, 155, 156, 156, 157, 158, 158, 159, 160, 161, 161, 162, 163, 163, 164, 165, 166, 166,
    167, 168, 169, 169, 170, 171, 171, 172, 173, 174, 174, 175, 176, 177, 177, 178, 179, 179, 180, 181, 182,
    182, 183, 184, 185, 185, 186, 187, 187, 188, 189, 190, 190, 191, 192, 193, 193, 194, 195, 195, 196, 197,
    198, 198, 199, 200, 200, 201, 202, 203, 203, 204, 205, 206, 206, 207, 208, 208, 209, 210, 211, 211, 212,
    213, 214, 214, 215, 216, 216, 217, 218, 219, 219, 220, 221, 222, 222, 223, 224, 224, 225, 226, 227, 227,
    228, 229, 230, 230, 231, 232, 232, 233, 234, 235, 235, 236, 237, 238, 238, 239, 240, 240, 241, 242, 243,
    243, 244, 245, 245, 246, 247, 248, 248, 249, 250, 251, 251, 252, 253, 253, 254, 255, 256, 256, 257, 258,
    259, 259, 260, 261, 261, 262, 263, 264, 264, 265, 266, 267, 267, 268, 269, 269, 270, 271, 272, 272, 273,
    274, 275, 275, 276, 277, 277, 278, 279, 280, 280, 281, 282, 282, 283, 284, 285, 285, 286, 287, 288, 288,
    289, 290, 290, 291, 292, 293, 293, 294, 295, 296, 296, 297, 298, 298, 299, 300, 301, 301, 302, 303, 304,
    304, 305, 306, 306, 307, 308, 309, 309, 310, 311, 312, 312, 313, 314, 314, 315, 316, 317, 317, 318, 319
];